<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            new FOS\UserBundle\FOSUserBundle(),
            new Rollerworks\Bundle\MultiUserBundle\RollerworksMultiUserBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle(),
            new Gfreeau\Bundle\GetJWTBundle\GfreeauGetJWTBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Nelmio\CorsBundle\NelmioCorsBundle(),
            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new Ci\RestClientBundle\CiRestClientBundle(),
            new Leezy\PheanstalkBundle\LeezyPheanstalkBundle(),

            new Gfreeau\Bundle\CustomValidationPathBundle\GfreeauCustomValidationPathBundle(),
            new DTag\Bundles\AdminApiBundle\DTagAdminApiBundle(),
            new DTag\Bundles\ApiBundle\DTagApiBundle(),
            new DTag\Bundles\UserBundle\DTagUserBundle(),
            new DTag\Bundles\UserSystem\AdminBundle\DTagUserSystemAdminBundle(),
            new DTag\Bundles\UserSystem\ManagerBundle\DTagUserSystemManagerBundle(),
            new DTag\Bundles\UserSystem\StaffBundle\DTagUserSystemStaffBundle(),
            new DTag\Bundles\UserSystem\ShipperBundle\DTagUserSystemShipperBundle(),
            new DTag\Bundles\UserSystem\CustomerBundle\DTagUserSystemCustomerBundle(),
            new DTag\Bundles\ReportApiBundle\DTagBundlesReportApiBundle(),
            new Liuggio\ExcelBundle\LiuggioExcelBundle(),

        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }

//    public function getCacheDir()
//    {
//        if ($this->isRunningOnDevelopmentVM()) {
//            return '/dev/shm/vietpost-api/cache/' .  $this->environment;
//        }
//        return parent::getCacheDir();
//    }
//
//    public function getLogDir()
//    {
//        if ($this->isRunningOnDevelopmentVM()) {
//            return '/dev/shm/vietpost-api/logs';
//        }
//        return parent::getLogDir();
//    }

    /**
     * Checks that an environment variable is set and has a truthy value
     *
     * @param string $variable
     * @return bool
     */
    protected function checkForEnvironmentVariable($variable)
    {
        return isset($_SERVER[$variable]) && (bool) $_SERVER[$variable];
    }
    /**
     * The application is in development mode if an environment variable D_TAG_DEV is set
     * and an environment variable D_TAG_PROD is not set
     *
     * @return bool
     */
    protected function isRunningOnDevelopmentVM()
    {
        $notProd = !$this->checkForEnvironmentVariable('MARKET_PROD') ;

        return $notProd;
    }
}

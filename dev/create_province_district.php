<?php
namespace DTag\dev;
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 4/17/16
 * Time: 9:56 AM
 */


use AppKernel;
use Doctrine\ORM\EntityManagerInterface;
use DTag\Entity\Core\Province;
use DTag\Entity\Core\District;
use DTag\Model\Core\DistrictInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

$loader = require_once __DIR__ . '/../app/autoload.php';
require_once __DIR__ . '/../app/AppKernel.php';
$kernel = new AppKernel('dev', true);
$kernel->boot();
/** @var ContainerInterface $container */
$container = $kernel->getContainer();
/** @var EntityManagerInterface $em */
$em = $container->get('doctrine.orm.entity_manager');
$em->getConnection()->getConfiguration()->setSQLLogger(null);


$hanoi = new Province();
$hanoi->setName("Hà Nội")->setShortName("HN")->setType(0);

$q1 = new District();
$q1->setName('Ba Đình')->setShortName('BD')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_CITY);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Bắc Từ Liêm')->setShortName('BTL')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_1);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Cầu Giấy')->setShortName('CG')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_CITY);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Đống Đa')->setShortName('DD')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_CITY);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Hà Đông')->setShortName('HD')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_1);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Hai Bà Trưng')->setShortName('HBT')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_CITY);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Hoàn Kiếm')->setShortName('HK')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_CITY);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Hoàng Mai')->setShortName('HM')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_1);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Long Biên')->setShortName('LB')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_1);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Nam Từ Liêm')->setShortName('NTL')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_1);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Tây Hồ')->setShortName('TH')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_1);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Thanh Xuân')->setShortName('TX')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_CITY);
$hanoi->addDistrict($q1);

//$q1 = new District();
//$q1->setName('Ba Vì')->setShortName('BV')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
//$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Chương Mỹ')->setShortName('CM')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Đan Phượng')->setShortName('DP')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Đông Anh')->setShortName('DA')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Gia Lâm')->setShortName('GL')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Mê Linh')->setShortName('ML')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Hoài Đức')->setShortName('HHD')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
$hanoi->addDistrict($q1);

//$q1 = new District();
//$q1->setName('Phú Xuyên')->setShortName('PX')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
//$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Mỹ Đức')->setShortName('MD')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Phúc Thọ')->setShortName('PT')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Quốc Oai')->setShortName('QO')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Sóc Sơn')->setShortName('SS')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
$hanoi->addDistrict($q1);

//$q1 = new District();
//$q1->setName('Thạch Thất')->setShortName('TT')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
//$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Thanh Oai ')->setShortName('TO')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
$hanoi->addDistrict($q1);

$q1 = new District();
$q1->setName('Thanh Trì')->setShortName('TT')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
$hanoi->addDistrict($q1);

//$q1 = new District();
//$q1->setName('Thường Tín')->setShortName('TT2')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
//$hanoi->addDistrict($q1);

//$q1 = new District();
//$q1->setName('Ứng Hòa')->setShortName('UH')->setProvince($hanoi)->setType(DistrictInterface::DISTRICT_TYPE_SUB_2);
//$hanoi->addDistrict($q1);

//$tphcm = new Province();
//$tphcm->setName("TP HCM")->setShortName("TPHCM");
//
//$t1 = new District();
//$t1->setName('Quận 1')->setShortName('Q1')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận 2')->setShortName('Q2')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận 3')->setShortName('Q3')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận 4')->setShortName('Q4')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận 5')->setShortName('Q5')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận 6')->setShortName('Q6')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận 7')->setShortName('Q7')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận 8')->setShortName('Q8')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận 9')->setShortName('Q9')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận 10')->setShortName('Q10')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận 11')->setShortName('Q11')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận 12')->setShortName('Q12')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận Thủ Đức')->setShortName('TD')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận Gò Vấp')->setShortName('GV')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận Bình Thạnh')->setShortName('BT')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận Tân Bình')->setShortName('TB')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận Tân Phú')->setShortName('TP')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận Phú Nhuận')->setShortName('PN')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Quận Bình Tân')->setShortName('BT1')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Củ Chi')->setShortName('CC')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Hóc Môn')->setShortName('HM')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Bình Chánh')->setShortName('BC')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Nhà Bè')->setShortName('NB')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);
//
//$t1 = new District();
//$t1->setName('Cần Giờ')->setShortName('CG')
//    ->setProvince($tphcm);
//$tphcm->addDistrict($t1);

$em->persist($hanoi);
//$em->persist($tphcm);
$em->flush();
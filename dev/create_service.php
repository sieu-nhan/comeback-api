<?php
namespace DTag\dev;
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 4/17/16
 * Time: 9:56 AM
 */


use AppKernel;
use Doctrine\ORM\EntityManagerInterface;
use DTag\Entity\Core\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;

$loader = require_once __DIR__ . '/../app/autoload.php';
require_once __DIR__ . '/../app/AppKernel.php';
$kernel = new AppKernel('dev', true);
$kernel->boot();
/** @var ContainerInterface $container */
$container = $kernel->getContainer();
/** @var EntityManagerInterface $em */
$em = $container->get('doctrine.orm.entity_manager');
$em->getConnection()->getConfiguration()->setSQLLogger(null);

$service = new Service();
$service->setName("SHIP")->setShortName('SHIP')
    ->setDescription('Dịch vụ mặc định , chuyển hàng từ một địa điểm đến một địa điểm khác. Là dịch vụ mặc định của mọi đơn hàng.');
$em->persist($service);

unset($service);
$service = new Service();
$service->setName("COD")->setShortName('COD')
    ->setDescription('Dịch vụ thu tiền hộ khi có yêu cầu. Là dịch vụ giá trị gia tăng.');
$em->persist($service);

$em->flush();
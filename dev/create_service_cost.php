<?php
namespace DTag\dev;
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 4/17/16
 * Time: 10:38 AM
 */

use AppKernel;
use Doctrine\ORM\EntityManagerInterface;
use DTag\Entity\Core\InterDistrictServiceCost;
use DTag\Entity\Core\InterProvinceServiceCost;
use DTag\Model\Core\ProvinceInterface;
use DTag\Model\Core\ServiceInterface;
use DTag\Repository\Core\DistrictRepositoryInterface;
use DTag\Repository\Core\ProvinceRepositoryInterface;
use DTag\Repository\Core\ServiceRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

$loader = require_once __DIR__ . '/../app/autoload.php';
require_once __DIR__ . '/../app/AppKernel.php';
$kernel = new AppKernel('dev', true);
$kernel->boot();

/** @var ContainerInterface $container */
$container = $kernel->getContainer();

/** @var EntityManagerInterface $em */
$em = $container->get('doctrine.orm.entity_manager');
$em->getConnection()->getConfiguration()->setSQLLogger(null);

/** @var ProvinceRepositoryInterface */
$provinceRepository = $container->get('d_tag.repository.province');

/** @var DistrictRepositoryInterface */
$districtRepository = $container->get('d_tag.repository.district');

/** @var ServiceRepositoryInterface */
$serviceRepository = $container->get('d_tag.repository.service');

$allServices = $serviceRepository->findAll();
$allProvinces = $provinceRepository->findAll();
$allDistricts = $districtRepository->findAll();


/**
 * @var ServiceInterface $service
 */
foreach($allServices as $service) {
    for($i = 0; $i < count($allProvinces); $i++) {
        for($j = $i; $j < count($allProvinces); $j++) {
            $serviceCost = new InterProvinceServiceCost();
            $serviceCost->setSource($allProvinces[$i])
                ->setDestination($allProvinces[$j])
                ->setService($service)
                ->setCost(mt_rand(10000, 20000))
            ;

            $em->persist($serviceCost);
        }
    }

    for($i = 0; $i < count($allDistricts); $i++) {
        $serviceCost = new InterDistrictServiceCost();
        $serviceCost->setSource($allDistricts[$i])
            ->setService($service)
            ->setCost(mt_rand(10000, 20000))
        ;

        $em->persist($serviceCost);
    }
}

$em->flush();

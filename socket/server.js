var app = require('http').createServer(handler)
  , io = require('socket.io').listen(app)
  , url = require('url');
 
app.listen(8081);
 
function handler (req, res) {
	// parse URL
	var requestURL = url.parse(req.url, true);
 
	// if there is a message, send it
	if(requestURL.query.id)
		sendMessage(decodeURI(requestURL.query.id), decodeURI(requestURL.query.name));
 
	// end the response
	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.end("");
}
 
function sendMessage(id, name) {
	io.sockets.emit('notification', {'id': id, 'name': name});
}

<?php

namespace DTag\Behaviors;


use DTag\Exception\LogicException;
use DTag\Exception\NotFoundException;
use DTag\Model\User\Role\ManagerInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Rollerworks\Bundle\MultiUserBundle\Model\UserDiscriminatorInterface;

trait GetUserTrait {

    /**
     * @param $username
     * @return array ['user' => null, 'user_system' => null, 'user_manager' => null]
     */
    public function getUserByUsernameOrEmail($username)
    {
        $userManager = $this->getUserManager();
        $userDiscriminator = $this->getUserDiscriminator();
        $result = [
            'user' => null,
            'user_system' => null,
            'user_manager' => $userManager
        ];

        // try with manager
        $userDiscriminator->setCurrentUser('d_tag_user_system_manager');
        $user = $userManager->findUserByUsername($username);
        if (!$user instanceof ManagerInterface) {
            $user = $userManager->findUserByEmail($username);
        }

        if ($user instanceof ManagerInterface) {
            $result['user_system'] = 'd_tag_user_system_manager';
            $result['user'] = $user;
            return $result;
        }

        throw new NotFoundException('User not found');
    }

    public function getUserByConfirmationToken($token)
    {
        $userManager = $this->getUserManager();
        $userDiscriminator = $this->getUserDiscriminator();
        $result = [
            'user' => null,
            'user_system' => null,
            'user_manager' => $userManager
        ];

        // try with manager
        $userDiscriminator->setCurrentUser('d_tag_user_system_manager');
        $user = $userManager->findUserByConfirmationToken($token);
        if ($user instanceof ManagerInterface) {
            $result['user_system'] = 'd_tag_user_system_manager';
            $result['user'] = $user;
            return $result;
        }

        throw new NotFoundException('User not found');
    }

    public function getUserById($id)
    {
        $userManager = $this->getUserManager();
        $userDiscriminator = $this->getUserDiscriminator();
        $result = [
            'user' => null,
            'user_system' => null,
            'user_manager' => $userManager
        ];

        // try with manager
        $userDiscriminator->setCurrentUser('d_tag_user_system_manager');
        $user = $userManager->findUserBy(['id' => $id]);
        if ($user instanceof ManagerInterface) {
            $result['user_system'] = 'd_tag_user_system_manager';
            $result['user'] = $user;
            return $result;
        }

        throw new NotFoundException('User not found');
    }

//    /**
//     * @param UserInterface $user
//     * @return string
//     */
//    protected function getUserSystem(UserInterface $user) {
//        if ($user instanceof ManagerInterface) {
//            return 'd_tag_user_system_manager';
//        }
//
//
//        throw new LogicException(sprintf('Not support this user type %s yet', get_class($user)));
//    }

    /**
     * @return UserManagerInterface
     */
    protected abstract function getUserManager();

    /**
     * @return UserDiscriminatorInterface
     */
    protected abstract function getUserDiscriminator();
} 
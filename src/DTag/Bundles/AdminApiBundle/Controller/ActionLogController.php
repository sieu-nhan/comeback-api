<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 7/23/17
 * Time: 5:58 PM
 */

namespace DTag\Bundles\AdminApiBundle\Controller;

use DateTime;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use DTag\Bundles\AdminApiBundle\Entity\ActionLog;
use DTag\Bundles\AdminApiBundle\Repository\ActionLogRepositoryInterface;
use DTag\Exception\Report\InvalidDateException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;


/**
 * @Rest\RouteResource("Logs")
 */
class ActionLogController extends FOSRestController
{
    /**
     *
     * @Rest\Get("/logs")
     *
     * Get performance reports for the platform with optional date range.
     *
     * @ApiDoc(
     *  section = "admin",
     *  resource = true,
     *  statusCodes = {
     *      200 = "Returned when successful"
     *  }
     * )
     *
     * @Rest\QueryParam(name="startDate", requirements="\d{4}-\d{2}-\d{2}", nullable=true, description="Date of the log in format YYYY-MM-DD, defaults to the today")
     * @Rest\QueryParam(name="endDate", requirements="\d{4}-\d{2}-\d{2}", nullable=true, description="If you want a log range, set this to a date in format YYYY-MM-DD - must be older or equal than 'startDate'")
     * @Rest\QueryParam(name="rowOffset", requirements="\d+", default=0, description="Order number of rows to skip before rowLimit kicks in")
     * @Rest\QueryParam(name="rowLimit", requirements="\d+", default=10, description="Limit the amount of rows returned in the report, -1 for no limit")
     * @Rest\QueryParam(name="loginLogs", requirements="(true|false)", nullable=true)
     * @Rest\QueryParam(name="$managerId", requirements="\d+", nullable=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return array
     *
     * @throws InvalidDateException if start date is greater than end date
     */
    public function getAction(ParamFetcherInterface $paramFetcher)
    {
        $dateUtil = $this->get('d_tag.service.date_util');

        $startDate = $paramFetcher->get('startDate', true);
        $endDate = $paramFetcher->get('endDate', true);
        $rowLimit = $paramFetcher->get('rowLimit', true);
        $rowOffset = $paramFetcher->get('rowOffset', true);
        $loginLogs = filter_var($paramFetcher->get('loginLogs'), FILTER_VALIDATE_BOOLEAN);
        $managerId = $paramFetcher->get('$managerId', true);

        if (!$startDate) {
            $startDate = new DateTime('6 days ago');
        }

        if (!$endDate) {
            $endDate = new DateTime('today');
        }

        $startDate = $dateUtil->getDateTime($startDate, true);
        $endDate = $dateUtil->getDateTime($endDate, true);

        $rowOffset = intval($rowOffset);
        $rowLimit = intval($rowLimit);

        if (!$endDate) {
            $endDate = $startDate;
        }

        if ($startDate > $endDate) {
            throw new InvalidDateException('start date must be before the end date');
        }

        //for query, endDate must be after one day
        $endDate->add(new \DateInterval('P1D'));

        $manager = $managerId ? $this->get('d_tag_user.domain_manager.manager')->findManager($managerId) : null;

        /**
         * @var ActionLogRepositoryInterface
         */
        $actionLogRepository = $this->getDoctrine()->getRepository(ActionLog::class);

        $numRows = $actionLogRepository->getTotalRows($startDate, $endDate, $manager, $loginLogs);
        $logsList = $actionLogRepository->getLogsForDateRange($startDate, $endDate, $rowOffset, $rowLimit, $manager, $loginLogs);

        return new ActionLog($numRows, $logsList);
    }
}
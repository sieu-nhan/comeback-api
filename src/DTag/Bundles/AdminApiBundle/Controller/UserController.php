<?php

namespace DTag\Bundles\AdminApiBundle\Controller;

use DTag\Behaviors\GetUserTrait;
use DTag\Model\User\UserEntityInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use FOS\UserBundle\Model\UserManagerInterface;
use Rollerworks\Bundle\MultiUserBundle\Model\UserDiscriminatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class UserController extends FOSRestController implements ClassResourceInterface
{
    use GetUserTrait;

    /**
     * Get token for user only
     * @param $id
     * @return array
     */
    public function getTokenAction($id)
    {
        $userDescriptor = $this->getUserById($id);
        $user = $userDescriptor['user'];

        $jwtManager = $this->get('lexik_jwt_authentication.jwt_manager');
        $jwtTransformer = $this->get('d_tag_api.service.jwt_response_transformer');

        $tokenString = $jwtManager->create($user);

        return $jwtTransformer->transform(['token' => $tokenString], $user);
    }

    /**
     * @return UserManagerInterface
     */
    protected function getUserManager()
    {
        return $this->get('fos_user.user_manager');
    }

    /**
     * @return UserDiscriminatorInterface
     */
    protected function getUserDiscriminator()
    {
        return $this->get('rollerworks_multi_user.user_discriminator');
    }
}

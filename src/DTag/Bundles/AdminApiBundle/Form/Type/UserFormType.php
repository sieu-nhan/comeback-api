<?php

namespace DTag\Bundles\AdminApiBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use DTag\Bundles\UserSystem\CustomerBundle\Entity\User;
use DTag\Form\Type\AbstractRoleSpecificFormType;
use DTag\Model\User\Role\AdminInterface;
use DTag\Model\User\Role\CustomerInterface;
use DTag\Model\User\UserEntityInterface;

class UserFormType extends AbstractRoleSpecificFormType
{
    public function __construct(UserEntityInterface $userRole)
    {
        $this->setUserRole($userRole);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('plainPassword')
            ->add('name')
            ->add('gender')
            ->add('email')
            ->add('phone')
            ->add('address')
            ->add('district')
            ->add('bankName')
            ->add('branch')
            ->add('accountOwner')
            ->add('accountNumber')
        ;

        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                /** @var CustomerInterface $user */
                $user = $event->getData();

                $user->setActive(true);
            }
        );

        if($this->userRole instanceof AdminInterface){
            $builder
                ->add('enabled')
            ;
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => User::class,
                'validation_groups' => ['Admin', 'Default'],
            ])
        ;
    }

    public function getName()
    {
        return 'd_tag_form_admin_api_user';
    }
}
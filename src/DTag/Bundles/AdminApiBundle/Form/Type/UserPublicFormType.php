<?php

namespace DTag\Bundles\AdminApiBundle\Form\Type;

use DTag\Services\captcha\ValidateCaptchaServiceInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use DTag\Bundles\UserSystem\CustomerBundle\Entity\User;
use DTag\Form\Type\AbstractRoleSpecificFormType;
use DTag\Model\User\Role\AdminInterface;
use DTag\Model\User\Role\CustomerInterface;
use DTag\Model\User\UserEntityInterface;

class UserPublicFormType extends AbstractRoleSpecificFormType
{
    /**
     * @var ValidateCaptchaServiceInterface
     */
    private $captchaValidator;

    function __construct(ValidateCaptchaServiceInterface $captchaValidator)
    {
        $this->captchaValidator = $captchaValidator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('plainPassword')
            ->add('name')
            ->add('gender')
            ->add('email')
            ->add('phone')
            ->add('address')
            ->add('district')
            ->add('bankName')
            ->add('branch')
            ->add('accountOwner')
            ->add('accountNumber')
            ->add('recaptcha', null, array(
                'mapped' => false
            ))
        ;

        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                /** @var CustomerInterface $user */
                $user = $event->getData();

                $recaptcha = $event->getForm()->get('recaptcha')->getData();

                if (!$this->captchaValidator->validate($recaptcha)) {
                    $event->getForm()->get('recaptcha')->addError(new FormError('this value is not valid captcha'));
                    return;
                }

                $user->setActive(true);
                $user->setEnabled(true);
            }
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => User::class,
                'validation_groups' => ['Admin', 'Default'],
            ])
        ;
    }

    public function getName()
    {
        return 'd_tag_form_admin_api_user_public';
    }
}
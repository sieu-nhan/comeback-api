<?php

namespace DTag\Bundles\AdminApiBundle\Repository;

use DateTime;
use Doctrine\ORM\EntityRepository;
use DTag\Model\User\Role\ManagerInterface;

class ActionLogRepository extends EntityRepository implements ActionLogRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function getLogsForDateRange(DateTime $startDate, DateTime $endDate, $offset = 0, $limit = 10, ManagerInterface $manager = null, $loginLog = false)
    {
        $qb = $this->createLogsQueryBuilder($startDate, $endDate, $manager, $loginLog);

        if (is_int($offset)) {
            $qb->setFirstResult($offset);
        }

        if (is_int($limit)) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @inheritdoc
     */
    public function getTotalRows(DateTime $startDate, DateTime $endDate, ManagerInterface $manager = null, $loginLog = false)
    {
        $qb = $this->createLogsQueryBuilder($startDate, $endDate, $manager, $loginLog);

        $result = $qb->select('count(l)')->getQuery()->getSingleScalarResult();

        return $result !== null ? $result : 0;
    }

    /**
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param ManagerInterface $manager
     * @param bool $loginLog
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function createLogsQueryBuilder(DateTime $startDate, DateTime $endDate, ManagerInterface $manager = null, $loginLog = false)
    {
        $qb = $this->createQueryBuilder('l');

        $qb = $qb
            ->where($qb->expr()->between('l.createdAt', ':startDate', ':endDate'))
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->addOrderBy('l.id', 'desc');

        if (null !== $manager) {
            $qb->andWhere('l.user = :user')
                ->setParameter('user', $manager);
        }

        $qb->andWhere($loginLog ? 'l.action = :action' : 'l.action <> :action')
            ->setParameter('action', 'LOGIN');

        return $qb;
    }
}

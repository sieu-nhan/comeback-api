<?php

namespace DTag\Bundles\AdminApiBundle\Repository;
use DateTime;
use DTag\Model\User\Role\ManagerInterface;

interface ActionLogRepositoryInterface {

    /**
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param int $offset
     * @param int $limit
     * @param ManagerInterface $manager
     * @param bool $loginLog
     * @return array
     */
    public function getLogsForDateRange(DateTime $startDate, DateTime $endDate, $offset = 0, $limit = 10, ManagerInterface $manager = null, $loginLog = true);

    /**
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param ManagerInterface $manager
     * @param bool $loginLog
     * @return int
     */
    public function getTotalRows(DateTime $startDate, DateTime $endDate, ManagerInterface $manager = null, $loginLog = true);

}
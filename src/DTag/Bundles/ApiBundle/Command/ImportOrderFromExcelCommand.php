<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 19/07/2016
 * Time: 22:43
 */

namespace DTag\Bundles\ApiBundle\Command;


use DTag\Exception\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportOrderFromExcelCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('dt:import:order')
            ->addArgument('file', InputArgument::REQUIRED, 'file to import')
            ->setDescription('Import orders from excel file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument('file');
        if (empty($file) || !file_exists($file)) {
            throw new InvalidArgumentException('file not found');
        }

        $excelProcessor = $this->getContainer()->get('d_tag.services.excel_processor');
        $count = $excelProcessor->createOrdersFromFile($file);
        $output->writeln(sprintf('<info>%d orders imported successfully !</info>', $count));
    }
}
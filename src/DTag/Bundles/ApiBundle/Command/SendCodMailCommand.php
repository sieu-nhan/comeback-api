<?php

namespace DTag\Bundles\ApiBundle\Command;

use DTag\DomainManager\OrderManagerInterface;
use DTag\Model\Core\OrderInterface;
use DTag\Services\Core\SendMailServiceInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendCodMailCommand extends ContainerAwareCommand
{
    /**
     * Configure the CLI task
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('dt:send_mail:cod')
            ->setDescription('Scan all cod order to user')
        ;
    }

    /**
     * Execute the CLI task
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var OrderManagerInterface $orderManager
         */
        $orderManager = $this->getContainer()->get('d_tag.domain_manager.order');

        $startDate = new \DateTime('2 days ago');
        $orders = $orderManager->getOrderByShippingStatus(OrderInterface::SHIPPING_STATUS_CREATED, $startDate);

        // group orders by user
        $users = [];
        /**
         * @var OrderInterface $order
         */
        foreach($orders as $order) {
            $email = $order->getCreator()->getEmail();
            if (!in_array($email, $users)) {
                $users[$email][] = $order;
            }
        }

        /**
         * @var SendMailServiceInterface $mailSender
         */
        $mailSender = $this->getContainer()->get('d_tag.services.send_mail_service');

        foreach($users as $email => $orders) {

            $mailSender->send(sprintf("Chuyển tiền đơn hàng thành công"), $email, ':Emails:welcome.html.twig', array(
                'name' => "Giang Coffee",
                'orders' => $orders
            ));
        }
    }
}
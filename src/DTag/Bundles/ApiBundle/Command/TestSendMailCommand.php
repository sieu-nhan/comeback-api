<?php

namespace DTag\Bundles\ApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestSendMailCommand extends ContainerAwareCommand
{
    /**
     * Configure the CLI task
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('dt:mail:send')
            ->addArgument('email', InputArgument::REQUIRED)
            ->setDescription('Send an email to user')
        ;
    }

    /**
     * Execute the CLI task
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument('email');
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) ) {
            throw new \InvalidArgumentException('Invalid email address');
        }

        /**
         * @var \DTag\Bundles\ApiBundle\Mailer\Mailer $mailSender
         */
        $mailSender = $this->getContainer()->get('d_tag_api.mailer.mailer');
        $mailSender->sendContactEmailMessage('Subject here', 'Hoang Long', 'litpuvn@gmail.com', '0963830369', 'I like you');
    }
}
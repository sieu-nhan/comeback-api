<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 15/04/2016
 * Time: 21:44
 */

namespace DTag\Bundles\ApiBundle\Controller;


use DTag\Exception\InvalidArgumentException;
use DTag\Model\Core\DistrictInterface;
use DTag\Model\Core\ServiceInterface;
use DTag\Repository\Core\DistrictRepositoryInterface;
use DTag\Repository\Core\ServiceRepositoryInterface;
use DTag\Services\Core\AutoCostingServiceInterface;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Rest\RouteResource("Cost")
 * Class TrackingController
 * @package DTag\Bundles\ApiBundle\Controller
 */
class AutoCostingController extends FOSRestController
{

    /**
     * @Rest\QueryParam(name="senderDistrict", requirements="\d+", nullable=false)
     * @Rest\QueryParam(name="senderAddress", nullable=false)
     * @Rest\QueryParam(name="targetDistrict", requirements="\d+", nullable=false)
     * @Rest\QueryParam(name="receiverAddress", nullable=false)
     *
     * @Rest\QueryParam(name="senderPay", requirements="(true|false)", nullable=true)
     * @Rest\QueryParam(name="cod", nullable=true)
     *
     * @Rest\QueryParam(name="weight", nullable=false)
     * @Rest\QueryParam(name="height", nullable=true)
     * @Rest\QueryParam(name="length", nullable=true)
     */
    public function getAutocostAction(Request $request)
    {
        $params = $this->get('fos_rest.request.param_fetcher')->all($strict = true);

        $senderDistrictId = filter_var($params['senderDistrict'], FILTER_VALIDATE_INT);
        $senderAddress = filter_var($params['senderAddress']);
        $targetDistrictId = filter_var($params['targetDistrict'], FILTER_VALIDATE_INT);
        $receiverAddress = filter_var($params['receiverAddress']);

        $senderPay = filter_var($params['senderPay'], FILTER_VALIDATE_BOOLEAN);
        $cod = filter_var($params['cod'], FILTER_VALIDATE_FLOAT);

        $weight = filter_var($params['weight'], FILTER_VALIDATE_FLOAT);


        /** @var DistrictRepositoryInterface $districtRepository */
        $districtRepository = $this->get('d_tag.repository.district');

        /**
         * @var DistrictInterface $senderDistrict
         */
        $senderDistrict = $districtRepository->find($senderDistrictId);

        /**
         * @var DistrictInterface $targetDistrict
         */
        $targetDistrict = $districtRepository->find($targetDistrictId);

        $interDistrictServiceCostRepository = $this->get('d_tag.repository.inter_district_service_cost');

        // get cost normal
        $serviceAutoCosting = $this->get('d_tag.services.auto_costing_service');
        $serviceAutoCosting->setInterDistrictServiceCostRepository($interDistrictServiceCostRepository);
        $costNormal = $serviceAutoCosting->calculateServiceCostNormal($targetDistrict, $weight);

        // get distance
        $serviceAutoDistance = $this->get('d_tag.services.auto_calculating_distance_service');
        $distance = $serviceAutoDistance->AutoCalculatingDistance($targetDistrict, $receiverAddress, $senderDistrict, $senderAddress);

        // get cost urgent
        $costUrgent = $serviceAutoCosting->calculateServiceCostUrgent($targetDistrict, $distance, $weight);

        return ['distance' => $distance, 'costNormal' => $costNormal, 'costUrgent' => $costUrgent];
    }
}
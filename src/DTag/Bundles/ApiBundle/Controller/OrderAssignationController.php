<?php

namespace DTag\Bundles\ApiBundle\Controller;

use DTag\Bundles\UserBundle\DomainManager\ShipperManagerInterface;
use DTag\Exception\InvalidArgumentException;
use DTag\Model\Core\OrderAssignationInterface;
use DTag\Model\User\Role\ShipperInterface;
use DTag\Services\Core\AssignOrderServiceInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Util\Codes;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Rest\RouteResource("OrderAssignation")
 * Class DistrictController
 * @package DTag\Bundles\ApiBundle\Controller
 */
class OrderAssignationController extends RestControllerAbstract implements ClassResourceInterface
{
    /**
     *
     * Get all assignation
     *
     *
     * @ApiDoc(
     *  section = "get list assignation ",
     *  resource = true,
     *  statusCodes = {
     *      200 = "Returned when successful"
     *  }
     * )
     *
     * @return OrderAssignationInterface[]
     */
    public function cgetAction()
    {
        return $this->all();
    }

    /**
     * Get a single assignation for the given id
     *
     * @ApiDoc(
     *  section = "get list assignation",
     *  resource = true,
     *  statusCodes = {
     *      200 = "Returned when successful"
     *  }
     * )
     *
     * @param $id
     * @return OrderAssignationInterface
     */
    public function getAction($id)
    {
        return $this->one($id);
    }

    /**
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') or has_role('ROLE_STAFF')")
     * Create a district from the submitted data
     *
     * @ApiDoc(
     *  section = "order",
     *  resource = true,
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when the submitted data has errors"
     *  }
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postAction(Request $request)
    {
        $parameters = $request->request->all();
        if (!array_key_exists('shipper', $parameters)) {
            throw new InvalidArgumentException('"order" is mandatory');
        }

        if (!array_key_exists('order', $parameters)) {
            throw new InvalidArgumentException('"order" is mandatory');
        }

        if (!is_array($parameters['order'])) {
            throw new InvalidArgumentException('expect "order" to be an array');
        }

        /** @var ShipperManagerInterface $shipperManager */
        $shipperManager = $this->get('d_tag_user.domain_manager.shipper');
        $shipper = $shipperManager->find($parameters['shipper']);
        if (!$shipper instanceof ShipperInterface) {
            throw new InvalidArgumentException(sprintf('shipper "%d" does not exist'));
        }

        /** @var AssignOrderServiceInterface $assignOrderService */
        $assignOrderService = $this->get('d_tag.services.assign_order_service');
        $assignOrderService->assign($shipper, $parameters['order']);

        return $this->view(null, Codes::HTTP_CREATED);
    }

    /**
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') or has_role('ROLE_STAFF')")
     * Update an existing district from the submitted data or create a new order
     *
     * @ApiDoc(
     *  section = "order",
     *  resource = true,
     *  statusCodes = {
     *      201 = "Returned when the resource is created",
     *      204 = "Returned when successful",
     *      400 = "Returned when the submitted data has errors"
     *  }
     * )
     *
     * @param Request $request the request object
     * @param int $id the resource id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when the resource does not exist
     */
    public function putAction(Request $request, $id)
    {
        return $this->put($request, $id);
    }

    /**
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') or has_role('ROLE_STAFF')")
     *
     * Update an existing district from the submitted data or create a new district at a specific location
     *
     * @ApiDoc(
     *  section = "order",
     *  resource = true,
     *  statusCodes = {
     *      204 = "Returned when successful",
     *      400 = "Returned when the submitted data has errors"
     *  }
     * )
     *
     * @param Request $request the request object
     * @param int $id the resource id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when resource not exist
     */
    public function patchAction(Request $request, $id)
    {
        return $this->patch($request, $id);
    }

    /**
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') or has_role('ROLE_STAFF')")
     *
     * Delete an existing district
     *
     * @ApiDoc(
     *  section = "Customer",
     *  resource = true,
     *  statusCodes = {
     *      204 = "Returned when successful",
     *      400 = "Returned when the submitted data has errors"
     *  }
     * )
     *
     * @param int $id the resource id
     *
     * @return View
     *
     * @throws NotFoundHttpException when the resource not exist
     */
    public function deleteAction($id)
    {
        return $this->delete($id);
    }

    /**
     * @inheritdoc
     */
    protected function getResourceName()
    {
        return 'orderassignation';
    }

    /**
     * @inheritdoc
     */
    protected function getGETRouteName()
    {
        return 'api_1_get_orderassignation';
    }

    /**
     * @inheritdoc
     */
    protected function getHandler()
    {
        return $this->container->get('d_tag_api.handler.order_assignation');
    }
}
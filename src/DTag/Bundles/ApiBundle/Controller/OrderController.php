<?php

namespace DTag\Bundles\ApiBundle\Controller;

use DTag\Model\Core\OrderInterface;
use DTag\Model\User\Role\AdminInterface;
use DTag\Model\User\Role\CustomerInterface;
use DTag\Services\Core\ExcelProcessorInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class OrderController extends RestControllerAbstract implements ClassResourceInterface
{
    /**
     *
     * Get all orders
     *
     *
     * @ApiDoc(
     *  section = "get list customer",
     *  resource = true,
     *  statusCodes = {
     *      200 = "Returned when successful"
     *  }
     * )
     *
     * @return OrderInterface[]
     */
    public function cgetAction()
    {
        return $this->all();
    }


    /**
     * Get a single customer for the given id
     *
     * @ApiDoc(
     *  section = "Customer",
     *  resource = true,
     *  statusCodes = {
     *      200 = "Returned when successful"
     *  }
     * )
     *
     * @param int $id the resource id
     *
     * @return \DTag\Model\Core\OrderInterface
     * @throws NotFoundHttpException when the resource does not exist
     */
    public function getAction($id)
    {
        return $this->one($id);
    }

    /**
     * get orders by shipping status
     * @Rest\QueryParam(name="status", nullable=false, requirements="\d+", description="shiping status")
     * @Rest\QueryParam(name="startDate", nullable=true)
     * @Rest\QueryParam(name="endDate", nullable=true)
     * @Rest\QueryParam(name="customerId", requirements="\d+", nullable=true)
     *
     * @return mixed
     * @throws \Exception
     */
    public function getShipstatusAction()
    {
        $paramFetcher = $this->get('fos_rest.request.param_fetcher');
        $status = $paramFetcher->get('status');
        $startDate = $paramFetcher->get('startDate');
        $endDate = $paramFetcher->get('endDate');
        $customerId = $paramFetcher->get('customerId');
        $user = $this->get('d_tag_user.domain_manager.customer')->find($customerId);

        if (empty($startDate)) {
            $startDate = new \DateTime('2 days ago');
        } else {
            if (!preg_match('/\d{4}-\d{2}-\d{2}/', $startDate)) {
                throw new \Exception('invalid startDate');
            }

            $startDate = new \DateTime($startDate);
        }

        if (empty($endDate)) {
            $endDate = new \DateTime();
        } else {
            if (!preg_match('/\d{4}-\d{2}-\d{2}/', $endDate)) {
                throw new \Exception('invalid startDate');
            }

            $endDate = new \DateTime($endDate);
        }

        if(!$user instanceof CustomerInterface || !$this->getUser() instanceof AdminInterface) {
            $user = $this->getUser();
        }

        $orderManager = $this->get('d_tag.domain_manager.order');

        return $orderManager->getOrderByShippingStatus($user, $status, $startDate, $endDate);
    }

    /**
     * get orders by shipping status
     * @Rest\QueryParam(name="date", nullable=false)
     *
     * @return mixed
     * @throws \Exception
     */
    public function getReportAction()
    {
        $paramFetcher = $this->get('fos_rest.request.param_fetcher');
        $date = $paramFetcher->get('date');
        if (!preg_match('/\d{4}-\d{2}-\d{2}/', $date)) {
            throw new \Exception('invalid $date');
        }

        $date = new \DateTime($date);
        return $this->get('d_tag.domain_manager.order')->getReportByDay($date);
    }

    /**
     * get order by paid status
     *
     * @Rest\QueryParam(name="status", nullable=false, requirements="\d+", description="paid status")
     * @Rest\QueryParam(name="startDate", nullable=true)
     * @return mixed
     * @throws \Exception
     */
    public function getPaidstatusAction()
    {
        $paramFetcher = $this->get('fos_rest.request.param_fetcher');
        $status = $paramFetcher->get('status');
        $startDate = $paramFetcher->get('startDate');

        if (empty($startDate)) {
            $startDate = new \DateTime('2 days ago');
        } else {
            if (!preg_match('/\d{4}-\d{2}-\d{2}/', $startDate)) {
                throw new \Exception('invalid startDate');
            }

            $startDate = new \DateTime($startDate);
        }

        $orderManager = $this->get('d_tag.domain_manager.order');

        return $orderManager->getOrderByPaidStatus([$status], $startDate);
    }

    /**
     * get order by cod status
     *
     * @Rest\QueryParam(name="status", nullable=false, requirements="\d+", description="cod status")
     * @Rest\QueryParam(name="startDate", nullable=true)
     * @return mixed
     * @throws \Exception
     */
    public function getCodstatusAction()
    {
        $paramFetcher = $this->get('fos_rest.request.param_fetcher');
        $status = $paramFetcher->get('status');
        $startDate = $paramFetcher->get('startDate');

        if (empty($startDate)) {
            $startDate = new \DateTime('2 days ago');
        } else {
            if (!preg_match('/\d{4}-\d{2}-\d{2}/', $startDate)) {
                throw new \Exception('invalid startDate');
            }

            $startDate = new \DateTime($startDate);
        }

        $orderManager = $this->get('d_tag.domain_manager.order');

        return $orderManager->getOrderByCodStatus([$status], $startDate);
    }

    /**
     * Create a order from the submitted data
     *
     * @ApiDoc(
     *  section = "order",
     *  resource = true,
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when the submitted data has errors"
     *  }
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postAction(Request $request)
    {
        $this->post($request);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function postExcelAction(Request $request)
    {
        /** @var ExcelProcessorInterface $excelProcessor */
        $excelProcessor = $this->get('d_tag.services.excel_processor');
        $uploadRootDir = $this->container->getParameter('upload_root_directory');
        $file_path = '';

        foreach ($_FILES as $file) {
            $uploadFile = new UploadedFile($file['tmp_name'], $file['name'], $file['type'], $file['size'], $file['error'], $test = false);
            $baseName = uniqid('', true);
            $uploadFile->move($uploadRootDir,
                $baseName.substr($uploadFile->getClientOriginalName(), -4)
            );

            $file_path = join('/', array($uploadRootDir, $baseName.substr($uploadFile->getClientOriginalName(), -4)));
        }

        return $excelProcessor->createOrdersFromFile($file_path);
    }

    /**
     * Update an existing order from the submitted data or create a new order
     *
     * @ApiDoc(
     *  section = "order",
     *  resource = true,
     *  statusCodes = {
     *      201 = "Returned when the resource is created",
     *      204 = "Returned when successful",
     *      400 = "Returned when the submitted data has errors"
     *  }
     * )
     *
     * @param Request $request the request object
     * @param int $id the resource id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when the resource does not exist
     */
    public function putAction(Request $request, $id)
    {
        return $this->put($request, $id);
    }

    /**
     * Update an existing customer from the submitted data or create a new customer at a specific location
     *
     * @ApiDoc(
     *  section = "order",
     *  resource = true,
     *  statusCodes = {
     *      204 = "Returned when successful",
     *      400 = "Returned when the submitted data has errors"
     *  }
     * )
     *
     * @param Request $request the request object
     * @param int $id the resource id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when resource not exist
     */
    public function patchAction(Request $request, $id)
    {
        return $this->patch($request, $id);
    }

    /**
     * Delete an existing customer
     *
     * @ApiDoc(
     *  section = "Customer",
     *  resource = true,
     *  statusCodes = {
     *      204 = "Returned when successful",
     *      400 = "Returned when the submitted data has errors"
     *  }
     * )
     *
     * @param int $id the resource id
     *
     * @return View
     *
     * @throws NotFoundHttpException when the resource not exist
     */
    public function deleteAction($id)
    {
        return $this->delete($id);
    }

    protected function getResourceName()
    {
        return 'order';
    }

    protected function getGETRouteName()
    {
        return 'api_1_get_order';
    }

    protected function getHandler()
    {
        return $this->container->get('d_tag.handler.order');
    }
}
<?php

namespace DTag\Bundles\ApiBundle\Controller;

use DTag\Model\Core\ProvinceInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Rest\RouteResource("Province")
 * Class ProvinceController
 * @package DTag\Bundles\ApiBundle\Controller
 */
class ProvinceController extends RestControllerAbstract implements ClassResourceInterface
{
    /**
     * Get all provinces
     *
     * @ApiDoc(
     *  section = "get list province",
     *  resource = true,
     *  statusCodes = {
     *      200 = "Returned when successful"
     *  }
     * )
     *
     * @return ProvinceInterface[]
     */
    public function cgetAction()
    {
        $provinceRepository = $this->get('d_tag.repository.province');
        return $provinceRepository->findBy($criteria = [], $orderBy = null, $limit = null, $offset = null);
    }

    /**
     * Get a single province for the given id
     *
     * @ApiDoc(
     *  section = "get list province",
     *  resource = true,
     *  statusCodes = {
     *      200 = "Returned when successful"
     *  }
     * )
     *
     * @param $id
     * @return ProvinceInterface
     */
    public function getAction($id)
    {
        return $this->one($id);
    }


    /**
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER')")
     * Create a district from the submitted data
     *
     * @ApiDoc(
     *  section = "order",
     *  resource = true,
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when the submitted data has errors"
     *  }
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postAction(Request $request)
    {
        $this->post($request);
    }


    /**
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER')")
     * Update an existing district from the submitted data or create a new order
     *
     * @ApiDoc(
     *  section = "order",
     *  resource = true,
     *  statusCodes = {
     *      201 = "Returned when the resource is created",
     *      204 = "Returned when successful",
     *      400 = "Returned when the submitted data has errors"
     *  }
     * )
     *
     * @param Request $request the request object
     * @param int $id the resource id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when the resource does not exist
     */
    public function putAction(Request $request, $id)
    {
        return $this->put($request, $id);
    }


    /**
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER')")
     *
     * Update an existing district from the submitted data or create a new district at a specific location
     *
     * @ApiDoc(
     *  section = "order",
     *  resource = true,
     *  statusCodes = {
     *      204 = "Returned when successful",
     *      400 = "Returned when the submitted data has errors"
     *  }
     * )
     *
     * @param Request $request the request object
     * @param int $id the resource id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when resource not exist
     */
    public function patchAction(Request $request, $id)
    {
        return $this->patch($request, $id);
    }


    /**
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER')")
     *
     * Delete an existing district
     *
     * @ApiDoc(
     *  section = "Customer",
     *  resource = true,
     *  statusCodes = {
     *      204 = "Returned when successful",
     *      400 = "Returned when the submitted data has errors"
     *  }
     * )
     *
     * @param int $id the resource id
     *
     * @return View
     *
     * @throws NotFoundHttpException when the resource not exist
     */
    public function deleteAction($id)
    {
        return $this->delete($id);
    }

    /**
     * @inheritdoc
     */
    protected function getResourceName()
    {
        return 'province';
    }

    /**
     * @inheritdoc
     */
    protected function getGETRouteName()
    {
        return 'api_1_get_province';
    }

    /**
     * @inheritdoc
     */
    protected function getHandler()
    {
        return $this->container->get('d_tag_api.handler.province');
    }
}
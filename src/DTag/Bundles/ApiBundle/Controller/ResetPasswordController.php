<?php

namespace DTag\Bundles\ApiBundle\Controller;

use DTag\Behaviors\GetUserTrait;
use DTag\Exception\NotFoundException;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Rollerworks\Bundle\MultiUserBundle\Model\UserDiscriminatorInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller managing the resetting of the password.
 *
 * This support 2 service: sendEmailAction (for send resetting email) & ResetAction (for do resetting password)
 */
class ResetPasswordController extends FOSRestController
{
    use GetUserTrait;
    /**
     * Request reset user password: submit form and send email. This used code from RollerWorksUserBundle Controller
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function sendEmailAction(Request $request)
    {
        try {
            $username = $request->request->get('username');
            $userDescriptor = $this->getUserByUsernameOrEmail($username);

            $userSystem = $userDescriptor['user_system'];
            /**
             * @var UserInterface $user
             */
            $user = $userDescriptor['user'];
            /**
             * @var UserManagerInterface $userManager
             */
            $userManager = $userDescriptor['user_manager'];
            $ttl = $this->container->getParameter(sprintf('%s.resetting.token_ttl', $userSystem));

            // check passwordRequest expired?
            if ($user->isPasswordRequestNonExpired($ttl)) {
                return $this->view('passwordAlreadyRequested', Codes::HTTP_ALREADY_REPORTED);
            }

            // generate confirmation Token
            if (null === $user->getConfirmationToken()) {
                $tokenGenerator = $this->container->get('fos_user.util.token_generator');
                $user->setConfirmationToken($tokenGenerator->generateToken());
            }

            // send resetting email
            $this->get('d_tag_api.mailer.mailer')->sendResettingEmailMessage($user);
            // update user
            $user->setPasswordRequestedAt(new \DateTime());
            $userManager->updateUser($user);

            return $this->view(null, Codes::HTTP_CREATED);
        }
        catch(NotFoundException $ex) {
        }

        return $this->view('invalid_username', Codes::HTTP_NOT_FOUND);
    }

    public function validateResetTokenAction(Request $request, $token)
    {
        //find user by token
        $userDescriptor = $this->getUserByConfirmationToken($token);
        $userSystem = $userDescriptor['user_system'];
        /**
         * @var UserInterface $user
         */
        $user = $userDescriptor['user'];

        if (null === $user) {
            return $this->view(null, Codes::HTTP_NOT_FOUND);
        }

        return $userSystem;
    }

    /**
     * Reset user password. This used code from FOSBundle Controller
     * @param Request $request
     * @param $token
     *
     * @return \FOS\RestBundle\View\View
     */
    public function resetAction(Request $request, $token)
    {
        //find user by token
        $userDescriptor = $this->getUserByConfirmationToken($token);
        $userSystem = $userDescriptor['user_system'];
        /**
         * @var UserInterface $user
         */
        $user = $userDescriptor['user'];
        /**
         * @var UserManagerInterface $userManager
         */
        $userManager = $userDescriptor['user_manager'];
        if (null === $user) {
            return $this->view(null, Codes::HTTP_NOT_FOUND);
        }

        $ttl = $this->container->getParameter(sprintf('%s.resetting.token_ttl', $userSystem));
        if (!$user->isPasswordRequestNonExpired($ttl)) {
            return $this->view(null, Codes::HTTP_REQUEST_TIMEOUT);
        }

        //using an event FOSUserEvents::SECURITY_IMPLICIT_LOGIN for RollerWorks auto setting user system as user_system_publisher (userDiscriminator->setCurrentUser(...))
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(FOSUserEvents::SECURITY_IMPLICIT_LOGIN, new UserEvent($user, $request));

        //create form factory, set user discriminator as d_tag_user_system_manager
        $formFactory = $this->get('rollerworks_multi_user.resetting.form.factory');

        //create form
        /** @var FormInterface $form */
        $form = $formFactory->createForm();
        $form->setData($user);

        //form handling request
        $form->handleRequest($request);

        //validate form and then update to db for publisher
        if ($form->isValid()) {
            $userManager->updateCanonicalFields($user);
            $user->setConfirmationToken(null);
            $user->setPasswordRequestedAt(null);
            $user->setEnabled(true);
            $userManager->updateUser($user);

            return $this->view(null, Codes::HTTP_ACCEPTED);
        }

        return $this->view($form->getErrors(), Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @return UserManagerInterface
     */
    protected function getUserManager()
    {
        return $this->get('fos_user.user_manager');
    }

    /**
     * @return UserDiscriminatorInterface
     */
    protected function getUserDiscriminator()
    {
        return $this->get('rollerworks_multi_user.user_discriminator');
    }
}

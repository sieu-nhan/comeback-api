<?php
namespace DTag\Bundles\ApiBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use DTag\Entity\Core\InterDistrictServiceCost;
use DTag\Model\Core\OrderInterface;
use DTag\Services\Core\AutoCalculatingDistanceServiceInterface;
use DTag\Services\Core\AutoCostingServiceInterface;

class UpdateCostListener {

    /**
     * @var AutoCostingServiceInterface
     */
    protected $autoCosting;

    /**
     * @var AutoCalculatingDistanceServiceInterface
     */
    protected $calculatingDistance;

    public function __construct(AutoCostingServiceInterface $autoCosting, AutoCalculatingDistanceServiceInterface $calculatingDistance)
    {
        $this->autoCosting = $autoCosting;
        $this->calculatingDistance = $calculatingDistance;
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $interDistrictServiceCostRepository = $args->getEntityManager()->getRepository(InterDistrictServiceCost::class);
        $this->autoCosting->setInterDistrictServiceCostRepository($interDistrictServiceCostRepository);

        $entity = $args->getEntity();
        if(!$entity instanceof OrderInterface) {
            return;
        }

        $this->updateCost($entity);
    }

    /**
     * handle event postPersist one site, this auto add site to SourceReportSiteConfig & SourceReportEmailConfig.
     *
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $interDistrictServiceCostRepository = $args->getEntityManager()->getRepository(InterDistrictServiceCost::class);
        $this->autoCosting->setInterDistrictServiceCostRepository($interDistrictServiceCostRepository);

        $entity = $args->getEntity();
        if(!$entity instanceof OrderInterface) {
            return;
        }

        $this->updateCost($entity);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if(!$entity instanceof OrderInterface) {
            return;
        }

        // push notification
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, sprintf('http://localhost:8081?id=%d&name=%s', $entity->getId(), urlencode($entity->getSenderName())));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
    }

    protected function updateCost(OrderInterface $order)
    {
        $distance = $this->calculatingDistance->AutoCalculatingDistance($order->getTargetDistrict(), $order->getReceiverAddress(), $order->getSenderDistrict(), $order->getSenderAddress());
        if($order->isUrgent() == false) {
            $shippingCost = $this->autoCosting->calculateServiceCostNormal($order->getTargetDistrict(), $order->getWeight());
        } else {
            $shippingCost = $this->autoCosting->calculateServiceCostUrgent($order->getTargetDistrict(), $distance, $order->getWeight());
        }

        $order->setDistance($distance);
        $order->setShippingCost($shippingCost);

        if($order->isSenderPay() == false) {
            $order->setSenderIncome($order->getCod());
            $order->setTotalCost($order->getCod());
        } else {
            $order->setSenderIncome($order->getCod() + $shippingCost);
            $order->setTotalCost($order->getCod() + $shippingCost);
        }
    }
}
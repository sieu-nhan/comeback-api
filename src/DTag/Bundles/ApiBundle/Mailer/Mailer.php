<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DTag\Bundles\ApiBundle\Mailer;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 */
class Mailer extends \FOS\UserBundle\Mailer\Mailer
{
    public function __construct($mailer, UrlGeneratorInterface $router, EngineInterface $templating, array $parameters)
    {
        parent::__construct($mailer, $router, $templating, $parameters);
    }

    /**
     * Override for change resetting url
     * {@inheritdoc}
     */
    public function sendResettingEmailMessage(UserInterface $user)
    {
        $template = $this->parameters['resetting.template'];
        $resetLink = $this->parameters['password_resetting_link'];
        $url = $resetLink . '/' . $user->getConfirmationToken();
        $rendered = $this->templating->render($template, array(
            'user' => $user,
            'confirmationUrl' => $url
        ));

        $this->sendEmailMessage($rendered, $this->parameters['contact_email'], $user->getEmail());
    }

    /**
     * send contact email to contact assistant
     * @param $subject
     * @param $name
     * @param $email
     * @param $phone
     * @param $content
     */
    public function sendContactEmailMessage($subject, $name, $email, $phone, $content)
    {
        $template = $this->parameters['contact.template'];
        $rendered = $this->templating->render($template, array(
            'subject' => $subject,
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'content' => $content
        ));

        $fromEmail = $this->parameters['contact_email'];
        $toEmail = $this->parameters['contact_email'];

        $this->sendEmailMessage($rendered, $fromEmail, $toEmail);
    }
}

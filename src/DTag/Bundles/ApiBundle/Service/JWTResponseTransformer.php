<?php

namespace DTag\Bundles\ApiBundle\Service;

use DTag\Model\User\Role\CustomerInterface;
use DTag\Model\User\UserEntityInterface;

class JWTResponseTransformer
{
    public function transform(array $data, UserEntityInterface $user)
    {
        $data['id'] = $user->getId();
        $data['username'] = $user->getUsername();
        $data['userRoles'] = $user->getUserRoles();

        if($user instanceof CustomerInterface) {
            $data['name'] = $user->getName();
            $data['phone'] = $user->getPhone();
            $data['district'] = $user->getDistrict();
            $data['address'] = $user->getAddress();
        }

        return $data;
    }
}
<?php

namespace DTag\Bundles\ReportApiBundle\Controller;

use DTag\Exception\LogicException;
use DTag\Model\User\Role\AdminInterface;
use DTag\Model\User\Role\CustomerInterface;
use DTag\Model\User\Role\UserRoleInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class StatisticsController extends FOSRestController
{
    /**
     *
     * Get all payment item for date range
     *
     * @Rest\View(
     *      serializerGroups={"paymentItem.detail", "paymentType.detail", "receiptItem.detail", "receiptType.detail"}
     * )
     *
     * @Rest\Get("/statistics")
     *
     * @Rest\QueryParam(name="startDate", requirements="\d{4}-\d{2}-\d{2}", nullable=true)
     * @Rest\QueryParam(name="endDate", requirements="\d{4}-\d{2}-\d{2}", nullable=true)
     *
     * @ApiDoc(
     *  section = "get list payment item for date range",
     *  resource = true,
     *  statusCodes = {
     *      200 = "Returned when successful"
     *  }
     * )
     */
    public function getAdminDashboardAction()
    {
        $params = $this->get('fos_rest.request.param_fetcher')->all($strict = true);
        $dateUtil = $this->get('d_tag.service.date_util');

        $startDate = $dateUtil->getDateTime($params['startDate'], true);
        $endDate = $dateUtil->getDateTime($params['endDate']);

        if (!$endDate) {
            $endDate = $startDate;
        }

        if ($startDate > $endDate) {
            throw new LogicException('start date must be before the end date');
        }

        return $this->get('d_tag_api.service.get_statistics_for_date_range')->getStatisticForDateRange($startDate, $endDate);
    }

    /**
     *
     * @Rest\Get("/managermoney")
     *
     * @Rest\QueryParam(name="customerId", requirements="\d+", nullable=true)
     *
     * @ApiDoc(
     *  section = "get list payment item for date range",
     *  resource = true,
     *  statusCodes = {
     *      200 = "Returned when successful"
     *  }
     * )
     */
    public function getManagerMoneyAction()
    {
        $params = $this->get('fos_rest.request.param_fetcher')->all($strict = true);
        $customerId = $params['customerId'];
        $user = $this->get('d_tag_user.domain_manager.customer')->find($customerId);

        if(!$user instanceof CustomerInterface) {
            $user = $this->getUser();
        }

        return $this->get('d_tag_api.service.get_statistics_for_money')->getStatisticMoney($user);
    }
}

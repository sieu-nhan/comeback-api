<?php

namespace DTag\Bundles\UserBundle\Annotations\UserType;

use Doctrine\Common\Annotations\Annotation;

use DTag\Model\User\Role\ManagerInterface;

/**
 * @Annotation
 * @Target({"METHOD","CLASS"})
 */
class Manager implements UserTypeInterface
{
    public function getUserClass()
    {
        return ManagerInterface::class;
    }
}
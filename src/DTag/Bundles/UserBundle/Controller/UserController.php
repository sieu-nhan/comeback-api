<?php

namespace DTag\Bundles\UserBundle\Controller;

use DTag\Bundles\AdminApiBundle\Handler\UserHandlerInterface;
use DTag\Bundles\ApiBundle\Controller\RestControllerAbstract;
use DTag\Model\Core\DistrictInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Util\Codes;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @RouteResource("User")
 * Class UserController
 * @package DTag\Bundles\UserBundle\Controller
 */
class UserController extends RestControllerAbstract implements ClassResourceInterface
{
    /**
     * @param Request $request
     * @return \FOS\UserBundle\Model\UserInterface
     */
    public function postAction(Request $request)
    {
        return $this->post($request);
    }

    /**
     * @return string
     */
    protected function getResourceName()
    {
        return 'user';
    }

    /**
     * The 'get' route name to redirect to after resource creation
     *
     * @return string
     */
    protected function getGETRouteName()
    {
        return 'api_1_post_user';
    }

    /**
     * @return UserHandlerInterface
     */
    protected function getHandler()
    {
        return $this->container->get('d_tag_admin_api.handler.user_public');
    }
}

<?php

namespace DTag\Bundles\UserBundle\DomainManager;

use DTag\Model\User\Role\CustomerInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserInterface as FOSUserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use DTag\Model\User\UserEntityInterface;

/**
 * Most of the other handlers talk to doctrine directly
 * This one is wrapping the bundle-specific FOSUserBundle
 * whilst keep a consistent API with the other handlers
 */
class CustomerManager implements CustomerManagerInterface
{
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_CUSTOMER = 'ROLE_CUSTOMER';

    /**
     * @var UserManagerInterface
     */
    protected $FOSUserManager;

    public function __construct(UserManagerInterface $userManager)
    {
        $this->FOSUserManager = $userManager;
    }

    /**
     * @inheritdoc
     */
    public function supportsEntity($entity)
    {
        return is_subclass_of($entity, FOSUserInterface::class);
    }

    /**
     * @inheritdoc
     */
    public function save(FOSUserInterface $user)
    {
        $this->FOSUserManager->updateUser($user);
    }

    /**
     * @inheritdoc
     */
    public function delete(FOSUserInterface $user)
    {
        $this->FOSUserManager->deleteUser($user);
    }

    /**
     * @inheritdoc
     */
    public function createNew()
    {
        return $this->FOSUserManager->createUser();
    }

    /**
     * @inheritdoc
     */
    public function find($id)
    {
        return $this->FOSUserManager->findUserBy(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public function all($limit = null, $offset = null)
    {
        return $this->FOSUserManager->findUsers();
    }

    /**
     * @inheritdoc
     */
    public function allCustomers($limit = null, $offset = null)
    {
        $managers = array_filter($this->all($limit, $offset), function(UserEntityInterface $user) {
            return $user->hasRole(static::ROLE_CUSTOMER);
        });

        return array_values($managers);
    }

    /**
     * @inheritdoc
     */
    public function allActiveCustomers($limit = null, $offset = null) {
        $managers = array_filter($this->all($limit, $offset), function(UserEntityInterface $user) {
            return $user->hasRole(static::ROLE_CUSTOMER) && $user->isEnabled();
        });

        return array_values($managers);
    }

    /**
     * @inheritdoc
     */
    public function findCustomer($id)
    {
        $customer = $this->find($id);

        if (!$customer) {
            return false;
        }

        if (!$customer instanceof CustomerInterface) {
            return false;
        }

        return $customer;
    }

    /**
     * @inheritdoc
     */
    public function findUserByUsernameOrEmail($usernameOrEmail)
    {
        return $this->FOSUserManager->findUserByUsernameOrEmail($usernameOrEmail);
    }

    /**
     * @inheritdoc
     */
    public function updateUser(UserInterface $token)
    {
        $this->FOSUserManager->updateUser($token);
    }

    /**
     * @inheritdoc
     */
    public function findUserByConfirmationToken($token)
    {
        return $this->FOSUserManager->findUserByConfirmationToken($token);
    }

    public function updateCanonicalFields(UserInterface $user)
    {
        $this->FOSUserManager->updateCanonicalFields($user);
    }


}

<?php

namespace DTag\Bundles\UserSystem\AdminBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use DTag\Model\User\Role\AdminInterface;
use DTag\Model\User\UserEntityInterface;


class SetRoleAdminListener {

    const ROLE_ADMIN = 'ROLE_ADMIN';

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof AdminInterface) {
            return;
        }

        /**
         * @var UserEntityInterface $entity
         */

        if (!$entity->hasRole(static::ROLE_ADMIN)) {
            $entity->addRole(static::ROLE_ADMIN);
        }
    }
} 
<?php

namespace DTag\Bundles\UserSystem\CustomerBundle\Controller;

use DTag\Bundles\AdminApiBundle\Handler\UserHandlerInterface;
use DTag\Bundles\ApiBundle\Controller\RestControllerAbstract;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Rest\RouteResource("customers/current")
 */
class CustomerController extends RestControllerAbstract implements ClassResourceInterface
{
    /**
     * Get current broker
     *
     * @return \DTag\Bundles\UserBundle\Entity\User
     * @throws NotFoundHttpException when the resource does not exist
     */
    public function getAction()
    {
        $customerId = $this->get('security.context')->getToken()->getUser()->getId();

        return $this->one($customerId);
    }

    /**
     * Get current broker
     *
     * @Rest\Get("/customers/{id}", requirements={"id" = "\d+"})
     *
     * @return \DTag\Bundles\UserBundle\Entity\User
     * @throws NotFoundHttpException when the resource does not exist
     */
    public function getCustomerAction($id)
    {
        return $this->one($id);
    }

    /**
     * @Rest\Get("/customers/all")
     * @return array
     */
    public function cgetAction()
    {
        return $this->get('d_tag_user.domain_manager.customer')->allActiveCustomers();
    }

    /**
     *
     * @Rest\Patch("/customers/{id}", requirements={"id" = "\d+"})
     *
     * Update current broker from the submitted data
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when resource not exist
     */
    public function patchCustomerAction(Request $request, $id)
    {
        return $this->patch($request, $id);
    }

    /**
     *  @Rest\Post("/customers")
     *
     * @param Request $request
     * @return View|FormTypeInterface
     */
    public function postAction(Request $request)
    {
        return $this->post($request);
    }

    /**
     * Update current broker from the submitted data
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when resource not exist
     */
    public function patchAction(Request $request)
    {
        $brokerId = $this->get('security.context')->getToken()->getUser()->getId();

        return $this->patch($request, $brokerId);
    }

    /**
     * @inheritdoc
     */
    protected function getResourceName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    protected function getGETRouteName()
    {
        return 'customer_api_1_get_current';
    }

    /**
     * @return UserHandlerInterface
     */
    protected function getHandler()
    {
        return $this->container->get('d_tag_admin_api.handler.user');
    }
}

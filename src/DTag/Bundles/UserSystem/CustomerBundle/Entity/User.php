<?php

namespace DTag\Bundles\UserSystem\CustomerBundle\Entity;

use DTag\Bundles\UserBundle\Entity\User as BaseUser;
use DTag\Model\Core\DistrictInterface;
use DTag\Model\User\Role\CustomerInterface;

class User extends BaseUser implements CustomerInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $customerCode;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var boolean
     */
    protected $active;

    /**
     * @var boolean
     */
    protected $personal;

    /**
     * @var integer
     */
    protected $gender;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var DistrictInterface
     */
    protected $district;

    /**
     * @var string
     */
    protected $ward;

    /**
     * @var string
     */
    protected $address;

    /**
     * @var  string
     */
    protected $accountNumber;

    /**
     * @var string
     */
    protected $accountOwner;

    /**
     * @var string
     */
    protected $bankName;

    /**
     * @var string
     */
    protected $branch;

    public function getUser()
    {
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return DistrictInterface
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param DistrictInterface $district
     * @return integer
     */
    public function setDistrict(DistrictInterface $district)
    {
        $this->district = $district;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     * @return self
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     * @return self
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isPersonal()
    {
        return $this->personal;
    }

    /**
     * @param boolean $personal
     * @return self
     */
    public function setPersonal($personal)
    {
        $this->personal = $personal;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWard()
    {
        return $this->ward;
    }

    /**
     * @param mixed $ward
     * @return self
     */
    public function setWard($ward)
    {
        $this->ward = $ward;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param string $accountNumber
     * @return self
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountOwner()
    {
        return $this->accountOwner;
    }

    /**
     * @param string $accountOwner
     * @return self
     */
    public function setAccountOwner($accountOwner)
    {
        $this->accountOwner = $accountOwner;
        return $this;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     * @return self
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
        return $this;
    }

    /**
     * @return string
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param string $branch
     * @return self
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerCode()
    {
        return $this->customerCode;
    }

    /**
     * @param string $customerCode
     * @return self
     */
    public function setCustomerCode($customerCode)
    {
        $this->customerCode = $customerCode;
        return $this;
    }
}
<?php

namespace DTag\Bundles\UserSystem\CustomerBundle\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use DTag\Model\User\Role\CustomerInterface;
use DTag\Model\User\UserEntityInterface;

class SetCustomerRoleListener
{
    const ROLE_CUSTOMER = 'ROLE_CUSTOMER';

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof CustomerInterface) {
            return;
        }

        /**
         * @var UserEntityInterface $entity
         */

        $entity->setUserRoles(array(static::ROLE_CUSTOMER));
    }
} 
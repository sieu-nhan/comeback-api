<?php

namespace DTag\Bundles\UserSystem\ManagerBundle\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use DTag\Model\User\Role\ManagerInterface;
use DTag\Model\User\UserEntityInterface;

class SetManagerRoleListener
{
    const ROLE_MANAGER = 'ROLE_MANAGER';

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof ManagerInterface) {
            return;
        }

        /**
         * @var UserEntityInterface $entity
         */
        if (!$entity->hasRole(static::ROLE_MANAGER)) {
            $entity->addRole(static::ROLE_MANAGER);
        }
    }
} 
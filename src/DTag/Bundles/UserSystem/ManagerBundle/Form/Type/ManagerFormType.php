<?php

namespace DTag\Bundles\UserSystem\ManagerBundle\Form\Type;

use DTag\Bundles\UserSystem\ManagerBundle\Entity\Manager;
use DTag\Form\Type\AbstractRoleSpecificFormType;
use DTag\Model\User\Role\AdminInterface;
use DTag\Model\User\Role\ManagerInterface;
use DTag\Model\User\UserEntityInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class ManagerFormType extends AbstractRoleSpecificFormType
{
    public function __construct(UserEntityInterface $userRole)
    {
        $this->setUserRole($userRole);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('plainPassword')
            ->add('name')
            ->add('email')
            ->add('phone')
            ->add('address')
            ->add('gender');


        if ($this->userRole instanceof AdminInterface) {
            $builder
                ->add('enabled');
        }
    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Manager::class,
//                'validation_groups' => [['Admin', 'Default']],
            ]);
    }

    public function getName()
    {
        return 'd_tag_form_manager';
    }
}
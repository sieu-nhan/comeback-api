<?php

namespace DTag\Bundles\UserSystem\ShipperBundle;

final class DTagUserSystemShipperEvents
{
    const CHANGE_PASSWORD_INITIALIZE = 'd_tag_user_system_shipper.change_password.edit.initialize';
    const CHANGE_PASSWORD_SUCCESS = 'd_tag_user_system_shipper.change_password.edit.success';
    const CHANGE_PASSWORD_COMPLETED = 'd_tag_user_system_shipper.change_password.edit.completed';
    const GROUP_CREATE_INITIALIZE = 'd_tag_user_system_shipper.group.create.initialize';
    const GROUP_CREATE_SUCCESS = 'd_tag_user_system_shipper.group.create.success';
    const GROUP_CREATE_COMPLETED = 'd_tag_user_system_shipper.group.create.completed';
    const GROUP_DELETE_COMPLETED = 'd_tag_user_system_shipper.group.delete.completed';
    const GROUP_EDIT_INITIALIZE = 'd_tag_user_system_shipper.group.edit.initialize';
    const GROUP_EDIT_SUCCESS = 'd_tag_user_system_shipper.group.edit.success';
    const GROUP_EDIT_COMPLETED = 'd_tag_user_system_shipper.group.edit.completed';
    const PROFILE_EDIT_INITIALIZE = 'd_tag_user_system_shipper.profile.edit.initialize';
    const PROFILE_EDIT_SUCCESS = 'd_tag_user_system_shipper.profile.edit.success';
    const PROFILE_EDIT_COMPLETED = 'd_tag_user_system_shipper.profile.edit.completed';
    const REGISTRATION_INITIALIZE = 'd_tag_user_system_shipper.registration.initialize';
    const REGISTRATION_SUCCESS = 'd_tag_user_system_shipper.registration.success';
    const REGISTRATION_COMPLETED = 'd_tag_user_system_shipper.registration.completed';
    const REGISTRATION_CONFIRM = 'd_tag_user_system_shipper.registration.confirm';
    const REGISTRATION_CONFIRMED = 'd_tag_user_system_shipper.registration.confirmed';
    const RESETTING_RESET_INITIALIZE = 'd_tag_user_system_shipper.resetting.reset.initialize';
    const RESETTING_RESET_SUCCESS = 'd_tag_user_system_shipper.resetting.reset.success';
    const RESETTING_RESET_COMPLETED = 'd_tag_user_system_shipper.resetting.reset.completed';
    const SECURITY_IMPLICIT_LOGIN = 'd_tag_user_system_shipper.security.implicit_login';
}

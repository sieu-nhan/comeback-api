<?php

namespace DTag\Bundles\UserSystem\ShipperBundle\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use DTag\Model\User\Role\ShipperInterface;
use DTag\Model\User\UserEntityInterface;

class SetShipperRoleListener
{
    const ROLE_SHIPPER = 'ROLE_SHIPPER';

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof ShipperInterface) {
            return;
        }

        /**
         * @var UserEntityInterface $entity
         */

        $entity->setUserRoles(array(static::ROLE_SHIPPER));
    }
} 
<?php

namespace DTag\Bundles\UserSystem\StaffBundle\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use DTag\Model\User\Role\StaffInterface;
use DTag\Model\User\UserEntityInterface;

class SetStaffRoleListener
{
    const ROLE_STAFF = 'ROLE_STAFF';

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof StaffInterface) {
            return;
        }

        /**
         * @var UserEntityInterface $entity
         */

        $entity->setUserRoles(array(static::ROLE_STAFF));
    }
} 
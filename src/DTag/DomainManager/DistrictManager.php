<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 4/17/16
 * Time: 2:25 PM
 */

namespace DTag\DomainManager;


use Doctrine\Common\Persistence\ObjectManager;
use DTag\Model\Core\DistrictInterface;
use DTag\Model\ModelInterface;
use DTag\Repository\Core\DistrictRepositoryInterface;
use InvalidArgumentException;
use ReflectionClass;

class DistrictManager implements DistrictManagerInterface
{
    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @var DistrictRepositoryInterface
     */
    protected $repository;

    public function __construct(ObjectManager $om, DistrictRepositoryInterface $repository)
    {
        $this->om = $om;
        $this->repository = $repository;
    }

    /**
     * @inheritdoc
     */
    public function supportsEntity($entity)
    {
        return is_subclass_of($entity, DistrictInterface::class);
    }

    /**
     * @inheritdoc
     */
    public function save(ModelInterface $district)
    {
        if(!$district instanceof DistrictInterface) throw new InvalidArgumentException('expect DistrictInterface object');

        $this->om->persist($district);
        $this->om->flush();
    }

    /**
     * @inheritdoc
     */
    public function delete(ModelInterface $district)
    {
        if(!$district instanceof DistrictInterface) throw new InvalidArgumentException('expect DistrictInterface object');

        $this->om->remove($district);
        $this->om->flush();
    }

    /**
     * @inheritdoc
     */
    public function createNew()
    {
        $entity = new ReflectionClass($this->repository->getClassName());
        return $entity->newInstance();
    }

    /**
     * @inheritdoc
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @inheritdoc
     */
    public function all($limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria = [], $orderBy = null, $limit, $offset);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 4/17/16
 * Time: 2:25 PM
 */

namespace DTag\DomainManager;


use Doctrine\Common\Persistence\ObjectManager;
use DTag\Model\Core\DistrictInterface;
use DTag\Model\Core\InterDistrictServiceCostInterface;
use DTag\Model\ModelInterface;
use DTag\Repository\Core\InterDistrictServiceCostRepositoryInterface;
use InvalidArgumentException;
use ReflectionClass;

class InterDistrictServiceCostManager implements InterDistrictServiceCostManagerInterface
{
    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @var InterDistrictServiceCostRepositoryInterface
     */
    protected $repository;

    public function __construct(ObjectManager $om, InterDistrictServiceCostRepositoryInterface $repository)
    {
        $this->om = $om;
        $this->repository = $repository;
    }

    /**
     * @inheritdoc
     */
    public function supportsEntity($entity)
    {
        return is_subclass_of($entity, DistrictInterface::class);
    }

    /**
     * @inheritdoc
     */
    public function save(ModelInterface $order)
    {
        if(!$order instanceof InterDistrictServiceCostInterface) throw new InvalidArgumentException('expect InterDistrictServiceCostInterface object');

        $this->om->persist($order);
        $this->om->flush();
    }

    /**
     * @inheritdoc
     */
    public function delete(ModelInterface $venue)
    {
        if(!$venue instanceof InterDistrictServiceCostInterface) throw new InvalidArgumentException('expect InterDistrictServiceCostInterface object');

        $this->om->remove($venue);
        $this->om->flush();
    }

    /**
     * @inheritdoc
     */
    public function createNew()
    {
        $entity = new ReflectionClass($this->repository->getClassName());
        return $entity->newInstance();
    }

    /**
     * @inheritdoc
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @inheritdoc
     */
    public function all($limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria = [], $orderBy = null, $limit, $offset);
    }
}
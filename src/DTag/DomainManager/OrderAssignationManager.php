<?php

namespace DTag\DomainManager;

use Doctrine\Common\Persistence\ObjectManager;
use DTag\Exception\InvalidArgumentException;
use DTag\Model\Core\OrderAssignationInterface;
use DTag\Model\Core\OrderInterface;
use DTag\Model\ModelInterface;
use DTag\Repository\Core\OrderAssignationRepositoryInterface;
use ReflectionClass;

class OrderAssignationManager implements OrderAssignationManagerInterface
{
    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @var OrderAssignationRepositoryInterface
     */
    protected $repository;

    public function __construct(ObjectManager $om, OrderAssignationRepositoryInterface $repository)
    {
        $this->om = $om;
        $this->repository = $repository;
    }

    /**
     * @inheritdoc
     */
    public function supportsEntity($entity)
    {
        return is_subclass_of($entity, OrderAssignationInterface::class);
    }

    /**
     * @inheritdoc
     */
    public function save(ModelInterface $order)
    {
        if(!$order instanceof OrderAssignationInterface) throw new InvalidArgumentException('expect OrderAssignationInterface object');

        $this->om->persist($order);
        $this->om->flush();
    }

    /**
     * @inheritdoc
     */
    public function delete(ModelInterface $venue)
    {
        if(!$venue instanceof OrderAssignationInterface) throw new InvalidArgumentException('expect OrderAssignationInterface object');

        $this->om->remove($venue);
        $this->om->flush();
    }

    /**
     * @inheritdoc
     */
    public function createNew()
    {
        $entity = new ReflectionClass($this->repository->getClassName());
        return $entity->newInstance();
    }

    /**
     * @inheritdoc
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @inheritdoc
     */
    public function all($limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria = [], $orderBy = null, $limit, $offset);
    }

    /**
     * @inheritdoc
     */
    public function isOrderAssigned(OrderInterface $order)
    {
        return $this->repository->isOrderAssigned($order);
    }
}
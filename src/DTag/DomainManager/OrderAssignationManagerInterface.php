<?php

namespace DTag\DomainManager;

use DTag\Model\Core\OrderInterface;

interface OrderAssignationManagerInterface extends ManagerInterface
{
    /**
     * @param OrderInterface $order
     * @return boolean
     */
    public function isOrderAssigned(OrderInterface $order);
}
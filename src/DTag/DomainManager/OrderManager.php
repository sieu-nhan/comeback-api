<?php

namespace DTag\DomainManager;

use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use DTag\Model\Core\OrderInterface;
use DTag\Model\ModelInterface;
use DTag\Model\User\Role\CustomerInterface;
use DTag\Model\User\Role\ManagerInterface;
use DTag\Model\User\Role\StaffInterface;
use DTag\Model\User\Role\UserRoleInterface;
use DTag\Repository\Core\OrderRepositoryInterface;
use ReflectionClass;
use DTag\Exception\InvalidArgumentException;

class OrderManager implements OrderManagerInterface
{
    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @var OrderRepositoryInterface
     */
    protected $repository;

    public function __construct(ObjectManager $om, OrderRepositoryInterface $repository)
    {
        $this->om = $om;
        $this->repository = $repository;
    }

    /**
     * @inheritdoc
     */
    public function supportsEntity($entity)
    {
        return is_subclass_of($entity, OrderInterface::class);
    }

    /**
     * @inheritdoc
     */
    public function save(ModelInterface $order)
    {
        if(!$order instanceof OrderInterface) throw new InvalidArgumentException('expect OrderInterface object');

        $this->om->persist($order);
        $this->om->flush();
    }

    /**
     * @inheritdoc
     */
    public function delete(ModelInterface $venue)
    {
        if(!$venue instanceof OrderInterface) throw new InvalidArgumentException('expect VenueInterface object');

        $this->om->remove($venue);
        $this->om->flush();
    }

    /**
     * @inheritdoc
     */
    public function createNew()
    {
        $entity = new ReflectionClass($this->repository->getClassName());
        return $entity->newInstance();
    }

    /**
     * @inheritdoc
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @inheritdoc
     */
    public function all($limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria = [], $orderBy = null, $limit, $offset);
    }

    /**
     * @inheritdoc
     */
    public function getOrderForCustomer(CustomerInterface $customer, $limit = null, $offset = null)
    {
        return $this->repository->getOrderForCustomer($customer, $limit, $offset);
    }

    /**
     * @inheritdoc
     */
    public function getOrderForStaff(StaffInterface $staff, $limit = null, $offset = null)
    {
        return $this->repository->getOrderForStaff($staff, $limit = null, $offset = null);
    }

    /**
     * @inheritdoc
     */
    public function getOrderByShippingStatus(UserRoleInterface $user, $status, $startDate = null, $endDate = null)
    {
        return $this->repository->getOrderByShippingStatus($user, $status, $startDate, $endDate);
    }

    /**
     * @inheritdoc
     */
    public function getOrderByPaidStatus($status, $startDate = null, $endDate = null)
    {
        return $this->repository->getOrderByPaidStatus($status, $startDate, $endDate);
    }

    /**
     * @inheritdoc
     */
    public function getOrderByCodStatus($status, $startDate = null, $endDate = null)
    {
        return $this->repository->getOrderByCodStatus($status, $startDate, $endDate);
    }

    public function getReportByDay(DateTime $date)
    {
        return $this->repository->getReportByDay($date);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 13/04/2016
 * Time: 23:19
 */

namespace DTag\Entity\Core;
use DTag\Model\Core\District as DistrictModel;
use DTag\Model\Core\ProvinceInterface;

class District extends DistrictModel
{
    /**
     * @var integer
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $shortName;
    /**
     * @var ProvinceInterface
     */
    protected $province;

    /**
     * @var integer
     */
    protected $type;
}
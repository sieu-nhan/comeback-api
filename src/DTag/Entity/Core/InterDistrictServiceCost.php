<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 13/04/2016
 * Time: 23:22
 */

namespace DTag\Entity\Core;
use DTag\Model\Core\DistrictInterface;
use DTag\Model\Core\InterDistrictServiceCost as InterDistrictServiceCostModel;

class InterDistrictServiceCost extends InterDistrictServiceCostModel
{
    /**
     * @var integer
     */
    protected $id;
    /**
     * @var DistrictInterface
     */
    protected $district;
    /**
     * @var float
     */
    protected $cost;
}
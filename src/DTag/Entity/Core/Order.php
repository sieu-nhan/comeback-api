<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 12/04/2016
 * Time: 23:22
 */

namespace DTag\Entity\Core;
use DTag\Model\Core\DistrictInterface;
use DTag\Model\Core\Order as OrderModel;
use DTag\Model\User\UserEntityInterface;

class Order extends OrderModel
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $orderId;

    /**
     * @var string
     */
    protected $senderName;

    /**
     * @var string
     */
    protected $senderAddress;

    /**
     * @var string
     */
    protected $senderPhone;

    /**
     * @var string
     */
    protected $receiverName;

    /**
     * @var string
     */
    protected $receiverAddress;

    /**
     * @var string
     */
    protected $receiverPhone;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var float
     */
    protected $shippingCost;

    /**
     * @var float
     */
    protected $cod;

    /**
     * @var integer
     */
    protected $codStatus;

    /**
     * @var float
     */
    protected $vat;

    /**
     * @var float
     */
    protected $totalCost;

    /**
     * specify who will be charged for the shipping cost
     * @var boolean
     */
    protected $senderPay;

    /**
     * money must transfer to sender
     * COD +/- shipping cost
     * @var float
     */
    protected $senderIncome;

    /**
     * in kilogram
     * @var float
     */
    protected $weight;

    /**
     * in centimeter
     * @var float
     */
    protected $height;

    /**
     * in centimeter
     * @var float
     */
    protected $width;

    /**
     * in centimeter
     * @var float
     */
    protected $length;

    /**
     * the categories of accompanied good
     * @var integer
     */
    protected $goodType;

    /**
     * @var boolean
     */
    protected $urgent;

    /**
     * @var integer
     */
    protected $shippingStatus;

    /**
     * @var integer
     */
    protected $paidStatus;

    /**
     * @var UserEntityInterface
     */
    protected $creator;

    /**
     * where to deliver the goods
     * @var DistrictInterface;
     */
    protected $targetDistrict;

    /**
     * where to deliver the goods
     * @var DistrictInterface;
     */
    protected $senderDistrict;

    protected  $distance;
}
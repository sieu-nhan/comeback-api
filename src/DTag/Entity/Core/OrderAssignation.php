<?php


namespace DTag\Entity\Core;

use DTag\Model\Core\OrderAssignation as OrderAssignationModel;
class OrderAssignation extends OrderAssignationModel
{
    protected $id;
    protected $order;
    protected $shipper;
    protected $createdAt;
    protected $deletedAt;
    protected $updatedAt;
}
<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 13/04/2016
 * Time: 23:08
 */

namespace DTag\Entity\Core;
use DTag\Model\Core\DistrictInterface;
use DTag\Model\Core\Province as ProvinceModel;

class Province extends ProvinceModel
{
    /**
     * @var integer
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $shortName;
    /**
     * @var DistrictInterface[]
     */
    protected $districts;

    /**
     * @var integer
     */
    protected $type;
}
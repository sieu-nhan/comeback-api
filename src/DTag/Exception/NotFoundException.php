<?php

namespace DTag\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NotFoundException extends NotFoundHttpException{

} 
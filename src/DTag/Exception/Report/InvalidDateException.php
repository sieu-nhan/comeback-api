<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 7/23/17
 * Time: 6:02 PM
 */

namespace DTag\Exception\Report;


use Symfony\Component\Form\Exception\InvalidArgumentException;

class InvalidDateException extends InvalidArgumentException
{}
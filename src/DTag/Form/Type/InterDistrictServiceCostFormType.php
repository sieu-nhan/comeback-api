<?php

namespace DTag\Form\Type;

use DTag\Entity\Core\InterDistrictServiceCost;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InterDistrictServiceCostFormType extends AbstractRoleSpecificFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cost')
            ->add('district')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => InterDistrictServiceCost::class,
            ])
        ;
    }

    public function getName()
    {
        return 'd_tag_form_interdistrictservicecost';
    }
}
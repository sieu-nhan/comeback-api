<?php

namespace DTag\Form\Type;

use DTag\Entity\Core\OrderAssignation;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OrderAssignationFormType extends AbstractRoleSpecificFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('order')
            ->add('shipper')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => OrderAssignation::class,
            ])
        ;
    }

    public function getName()
    {
        return 'd_tag_form_order_assignation';
    }
}
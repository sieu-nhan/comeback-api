<?php

namespace DTag\Form\Type;

use DTag\Model\Core\OrderInterface;
use DTag\Model\User\Role\AdminInterface;
use DTag\Model\User\Role\CustomerInterface;
use DTag\Model\User\Role\ManagerInterface;
use DTag\Model\User\Role\StaffInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use DTag\Entity\Core\Order;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormError;

class OrderFormType extends AbstractRoleSpecificFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('orderId')
            ->add('senderName')
            ->add('senderAddress')
            ->add('senderPhone')
            ->add('senderDistrict')
            ->add('receiverName')
            ->add('receiverAddress')
            ->add('receiverPhone')
            ->add('targetDistrict')
            ->add('senderPay')
            ->add('cod')
            ->add('vat')
            ->add('width')
            ->add('weight')
            ->add('height')
            ->add('length')
            ->add('urgent')
            ->add('description')
        ;

        if ($this->userRole instanceof AdminInterface ||
            $this->userRole instanceof ManagerInterface ||
            $this->userRole instanceof StaffInterface
        ) {
            $builder
                ->add('shippingStatus')
                ->add('paidStatus');
        }

        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                /** @var OrderInterface $order */
                $order = $event->getData();

                if($order->getId() === null) {
                    if ($order->getCreator() instanceof CustomerInterface) {
                        $order->setShippingStatus(OrderInterface::SHIPPING_STATUS_CREATED);
                    }

                    $order->setOrderId(strtoupper(uniqid("")))
                        ->setPaidStatus(OrderInterface::PAID_STATUS_UNPAID)
                        ->setCodStatus(OrderInterface::COD_STATUS_WAIT_FOR_TRANSFER)
                    ;

                }
            }
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Order::class,
            ])
        ;
    }

    public function getName()
    {
        return 'd_tag_form_order';
    }
}
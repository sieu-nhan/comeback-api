<?php

namespace DTag\Handler\Handlers\Core\Admin;

use DTag\Handler\Handlers\Core\ManagerHandlerAbstract;
use DTag\Model\User\Role\AdminInterface;
use DTag\Model\User\Role\UserRoleInterface;

/**
 * Not using a RoleHandlerInterface because this Handler is local
 * to the admin api bundle. All routes to this bundle are protected in app/config/security.yml
 */
class ManagerHandler extends ManagerHandlerAbstract
{
    /**
     * @inheritdoc
     */
    public function supportsRole(UserRoleInterface $role)
    {
        return $role instanceof AdminInterface;
    }

    /**
     * @inheritdoc
     */
    public function all($limit = null, $offset = null)
    {
        return $this->getDomainManager()->allManagers($limit, $offset);
    }
}
<?php

namespace DTag\Handler\Handlers\Core\Admin;

use DTag\Handler\Handlers\Core\OrderAssignationHandlerAbstract;
use DTag\Model\User\Role\AdminInterface;
use DTag\Model\User\Role\UserRoleInterface;

/**
 * Not using a RoleHandlerInterface because this Handler is local
 * to the admin api bundle. All routes to this bundle are protected in app/config/security.yml
 */
class OrderAssignationHandler extends OrderAssignationHandlerAbstract
{
    /**
     * @inheritdoc
     */
    public function supportsRole(UserRoleInterface $role)
    {
        return $role instanceof AdminInterface;
    }

    /**
     * @inheritdoc
     */
    public function all($limit = null, $offset = null)
    {
        return $this->getDomainManager()->all($limit, $offset);
    }
}
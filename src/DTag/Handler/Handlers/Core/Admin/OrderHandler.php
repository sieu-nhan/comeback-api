<?php

namespace DTag\Handler\Handlers\Core\Admin;

use DTag\Handler\Handlers\Core\OrderHandlerAbstract;
use DTag\Model\Core\OrderInterface;
use DTag\Model\ModelInterface;
use DTag\Model\User\Role\AdminInterface;
use DTag\Model\User\Role\UserRoleInterface;

class OrderHandler extends OrderHandlerAbstract
{
    /**
     * @param UserRoleInterface $role
     * @return bool
     */
    public function supportsRole(UserRoleInterface $role)
    {
        return $role instanceof AdminInterface;
    }

    public function all($limit = null, $offset = null)
    {
        return $this->getDomainManager()->all($limit, $offset);
    }

    /**
     * @inheritdoc
     */
    protected function processForm(ModelInterface $order, array $parameters, $method = "PUT")
    {
        /** @var OrderInterface $order */
        if (null == $order->getCreator()) {
            $order->setCreator($this->getUserRole());
        }
        return parent::processForm($order, $parameters, $method);
    }
}
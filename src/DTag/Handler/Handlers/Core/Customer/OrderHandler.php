<?php

namespace DTag\Handler\Handlers\Core\Customer;

use DTag\Exception\LogicException;
use DTag\Handler\Handlers\Core\OrderHandlerAbstract;
use DTag\Model\Core\OrderInterface;
use DTag\Model\ModelInterface;
use DTag\Model\User\Role\CustomerInterface;
use DTag\Model\User\Role\UserRoleInterface;

class OrderHandler extends OrderHandlerAbstract
{
    /**
     * @param UserRoleInterface $role
     * @return bool
     */
    public function supportsRole(UserRoleInterface $role)
    {
        return $role instanceof CustomerInterface;
    }

    /**
     * @inheritdoc
     * @return CustomerInterface
     * @throws LogicException
     */
    public function getUserRole()
    {
        $role = parent::getUserRole();
        if (!$role instanceof CustomerInterface) {
            throw new LogicException('userRole does not implement CustomerInterface');
        }
        return $role;
    }

    /**
     * @inheritdoc
     */
    protected function processForm(ModelInterface $order, array $parameters, $method = "PUT")
    {
        /** @var OrderInterface $order */
        if (null == $order->getCreator()) {
            $order->setCreator($this->getUserRole());
        }
        return parent::processForm($order, $parameters, $method);
    }

    /**
     * @inheritdoc
     */
    public function all($limit = null, $offset = null)
    {
        return $this->getDomainManager()->getOrderForCustomer($this->getUserRole(), $limit, $offset);
    }
}
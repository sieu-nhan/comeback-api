<?php

namespace DTag\Handler\Handlers\Core;

use DTag\DomainManager\DistrictManagerInterface;
use DTag\Handler\RoleHandlerAbstract;

abstract class DistrictHandlerAbstract extends RoleHandlerAbstract
{
    /**
     * @inheritdoc
     *
     * @return DistrictManagerInterface
     */
    protected function getDomainManager()
    {
        return parent::getDomainManager();
    }
}
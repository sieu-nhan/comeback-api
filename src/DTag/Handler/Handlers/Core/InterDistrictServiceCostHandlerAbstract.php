<?php

namespace DTag\Handler\Handlers\Core;

use DTag\DomainManager\InterDistrictServiceCostManagerInterface;
use DTag\Handler\RoleHandlerAbstract;

abstract class InterDistrictServiceCostHandlerAbstract extends RoleHandlerAbstract
{
    /**
     * @inheritdoc
     *
     * @return InterDistrictServiceCostManagerInterface
     */
    protected function getDomainManager()
    {
        return parent::getDomainManager();
    }
}
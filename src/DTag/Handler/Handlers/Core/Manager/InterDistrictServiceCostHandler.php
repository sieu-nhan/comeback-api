<?php

namespace DTag\Handler\Handlers\Core\Manager;

use DTag\Handler\Handlers\Core\InterDistrictServiceCostHandlerAbstract;
use DTag\Model\User\Role\ManagerInterface;
use DTag\Model\User\Role\UserRoleInterface;

/**
 * Not using a RoleHandlerInterface because this Handler is local
 * to the admin api bundle. All routes to this bundle are protected in app/config/security.yml
 */
class InterDistrictServiceCostHandler extends InterDistrictServiceCostHandlerAbstract
{
    /**
     * @inheritdoc
     */
    public function supportsRole(UserRoleInterface $role)
    {
        return $role instanceof ManagerInterface;
    }

    /**
     * @inheritdoc
     */
    public function all($limit = null, $offset = null)
    {
        return $this->getDomainManager()->all($limit, $offset);
    }
}
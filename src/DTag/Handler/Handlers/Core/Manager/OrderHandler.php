<?php

namespace DTag\Handler\Handlers\Core\Manager;

use DTag\Exception\LogicException;
use DTag\Handler\Handlers\Core\OrderHandlerAbstract;
use DTag\Model\Core\OrderInterface;
use DTag\Model\ModelInterface;
use DTag\Model\User\Role\ManagerInterface;
use DTag\Model\User\Role\UserRoleInterface;

class OrderHandler extends OrderHandlerAbstract
{
    /**
     * @param UserRoleInterface $role
     * @return bool
     */
    public function supportsRole(UserRoleInterface $role)
    {
        return $role instanceof ManagerInterface;
    }

    /**
     * @inheritdoc
     * @return ManagerInterface
     * @throws LogicException
     */
    public function getUserRole()
    {
        $role = parent::getUserRole();
        if (!$role instanceof ManagerInterface) {
            throw new LogicException('userRole does not implement ManagerInterface');
        }
        return $role;
    }

    /**
     * @inheritdoc
     */
    protected function processForm(ModelInterface $order, array $parameters, $method = "PUT")
    {
        /** @var OrderInterface $order */
        if (null == $order->getCreator()) {
            $order->setCreator($this->getUserRole());
        }
        return parent::processForm($order, $parameters, $method);
    }
}
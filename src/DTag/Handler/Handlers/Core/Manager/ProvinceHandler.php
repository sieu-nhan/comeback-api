<?php

namespace DTag\Handler\Handlers\Core\Manager;

use DTag\Handler\Handlers\Core\ProvinceHandlerAbstract;
use DTag\Model\User\Role\ManagerInterface;
use DTag\Model\User\Role\UserRoleInterface;

class ProvinceHandler extends ProvinceHandlerAbstract
{
    /**
     * @param UserRoleInterface $role
     * @return bool
     */
    public function supportsRole(UserRoleInterface $role)
    {
        return $role instanceof ManagerInterface;
    }
}
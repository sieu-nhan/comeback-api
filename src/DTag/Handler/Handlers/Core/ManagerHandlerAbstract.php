<?php

namespace DTag\Handler\Handlers\Core;

use DTag\Bundles\UserBundle\DomainManager\ManagerManagerInterface;
use DTag\Handler\RoleHandlerAbstract;

abstract class ManagerHandlerAbstract extends RoleHandlerAbstract
{
    /**
     * @inheritdoc
     *
     * @return ManagerManagerInterface
     */
    protected function getDomainManager()
    {
        return parent::getDomainManager();
    }
}
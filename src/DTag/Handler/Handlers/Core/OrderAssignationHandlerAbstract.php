<?php

namespace DTag\Handler\Handlers\Core;

use DTag\DomainManager\OrderAssignationManagerInterface;
use DTag\Handler\RoleHandlerAbstract;

abstract class OrderAssignationHandlerAbstract extends RoleHandlerAbstract
{
    /**
     * @inheritdoc
     *
     * @return OrderAssignationManagerInterface
     */
    protected function getDomainManager()
    {
        return parent::getDomainManager();
    }
}
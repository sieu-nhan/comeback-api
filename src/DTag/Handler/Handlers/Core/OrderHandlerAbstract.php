<?php

namespace DTag\Handler\Handlers\Core;

use DTag\DomainManager\OrderManagerInterface;
use DTag\Handler\RoleHandlerAbstract;

abstract class OrderHandlerAbstract extends RoleHandlerAbstract
{
    /**
     * @inheritdoc
     *
     * @return OrderManagerInterface
     */
    protected function getDomainManager()
    {
        return parent::getDomainManager();
    }
}
<?php

namespace DTag\Handler\Handlers\Core;

use DTag\DomainManager\ProvinceManagerInterface;
use DTag\Handler\RoleHandlerAbstract;

abstract class ProvinceHandlerAbstract extends RoleHandlerAbstract
{
    /**
     * @inheritdoc
     *
     * @return ProvinceManagerInterface
     */
    protected function getDomainManager()
    {
        return parent::getDomainManager();
    }
}
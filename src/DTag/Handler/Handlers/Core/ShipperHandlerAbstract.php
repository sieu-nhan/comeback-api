<?php

namespace DTag\Handler\Handlers\Core;

use DTag\Bundles\UserBundle\DomainManager\ShipperManagerInterface;
use DTag\Handler\RoleHandlerAbstract;

abstract class ShipperHandlerAbstract extends RoleHandlerAbstract
{
    /**
     * @inheritdoc
     *
     * @return ShipperManagerInterface
     */
    protected function getDomainManager()
    {
        return parent::getDomainManager();
    }
}
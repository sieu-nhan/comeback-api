<?php

namespace DTag\Handler\Handlers\Core\Customer;

use DTag\Handler\Handlers\Core\ProvinceHandlerAbstract;
use DTag\Model\User\Role\CustomerInterface;
use DTag\Model\User\Role\UserRoleInterface;

class ProvinceHandler extends ProvinceHandlerAbstract
{
    /**
     * @param UserRoleInterface $role
     * @return bool
     */
    public function supportsRole(UserRoleInterface $role)
    {
        return $role instanceof CustomerInterface;
    }
}
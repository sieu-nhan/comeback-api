<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 13/04/2016
 * Time: 22:41
 */

namespace DTag\Model\Core;


use DTag\Model\ModelInterface;

interface DistrictInterface extends ModelInterface
{
    const DISTRICT_TYPE_CITY = 1;
    const DISTRICT_TYPE_SUB_1 = 2;
    const DISTRICT_TYPE_SUB_2 = 3;

    /**
     * @param int $id
     * @return self
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return self
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getShortName();

    /**
     * @param string $shortName
     * @return self
     */
    public function setShortName($shortName);

    /**
     * @return ProvinceInterface
     */
    public function getProvince();

    /**
     * @param ProvinceInterface $province
     * @return self
     */
    public function setProvince($province);

    /**
     * @return int
     */
    public function getType();

    /**
     * @param int $type
     * @return self
     */
    public function setType($type);
}
<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 13/04/2016
 * Time: 23:00
 */

namespace DTag\Model\Core;


class InterDistrictServiceCost implements InterDistrictServiceCostInterface
{
    /**
     * @var integer
     */
    protected $id;
    /**
     * @var DistrictInterface
     */
    protected $district;
    /**
     * @var float
     */
    protected $cost;

    /**
     * InterDistrictServiceCost constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param float $cost
     * @return self
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return DistrictInterface
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param DistrictInterface $district
     * @return self
     */
    public function setDistrict($district)
    {
        $this->district = $district;
        return $this;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 13/04/2016
 * Time: 23:00
 */

namespace DTag\Model\Core;


use DTag\Model\ModelInterface;

interface InterDistrictServiceCostInterface extends ModelInterface
{
    /**
     * @param int $id
     * @return self
     */
    public function setId($id);

    /**
     * @return float
     */
    public function getCost();

    /**
     * @param float $cost
     * @return self
     */
    public function setCost($cost);

    /**
     * @return DistrictInterface
     */
    public function getDistrict();

    /**
     * @param DistrictInterface $district
     * @return self
     */
    public function setDistrict($district);
}
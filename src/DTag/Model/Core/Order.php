<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 12/04/2016
 * Time: 23:11
 */

namespace DTag\Model\Core;


use DTag\Model\User\UserEntityInterface;

class Order implements OrderInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $orderId;

    /**
     * @var string
     */
    protected $senderName;

    /**
     * @var string
     */
    protected $senderAddress;

    /**
     * @var string
     */
    protected $senderPhone;

    /**
     * @var string
     */
    protected $receiverName;

    /**
     * @var string
     */
    protected $receiverAddress;

    /**
     * @var string
     */
    protected $receiverPhone;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var float
     */
    protected $shippingCost;

    /**
     * @var float
     */
    protected $cod;

    /**
     * @var integer
     */
    protected $codStatus;

    /**
     * @var float
     */
    protected $vat;

    /**
     * @var float
     */
    protected $totalCost;

    /**
     * specify who will be charged for the shipping cost
     * @var boolean
     */
    protected $senderPay;

    /**
     * money must transfer to sender
     * COD +/- shipping cost
     * @var float
     */
    protected $senderIncome;

    /**
     * in kilogram
     * @var float
     */
    protected $weight;

    /**
     * in centimeter
     * @var float
     */
    protected $height;

    /**
     * in centimeter
     * @var float
     */
    protected $width;

    /**
     * in centimeter
     * @var float
     */
    protected $length;

    /**
     * the categories of accompanied good
     * @var integer
     */
    protected $goodType;

    /**
     * @var boolean
     */
    protected $urgent;

    /**
     * @var integer
     */
    protected $shippingStatus;

    /**
     * @var integer
     */
    protected $paidStatus;

    /**
     * @var UserEntityInterface
     */
    protected $creator;

    /**
     * where to deliver the goods
     * @var DistrictInterface;
     */
    protected $targetDistrict;

    /**
     * where to deliver the goods
     * @var DistrictInterface;
     */
    protected $senderDistrict;

    protected  $distance;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     */
    protected $deletedAt;


    /**
     * Order constructor.
     */
    public function __construct()
    {
    }

    public function getId()
    {
        return $this->id;
    }


    /**
     * @param int $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    /**
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param string $orderId
     * @return self
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * @param string $senderName
     * @return self
     */
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenderAddress()
    {
        return $this->senderAddress;
    }

    /**
     * @param string $senderAddress
     * @return self
     */
    public function setSenderAddress($senderAddress)
    {
        $this->senderAddress = $senderAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getReceiverName()
    {
        return $this->receiverName;
    }

    /**
     * @param string $receiverName
     * @return self
     */
    public function setReceiverName($receiverName)
    {
        $this->receiverName = $receiverName;
        return $this;
    }

    /**
     * @return string
     */
    public function getReceiverAddress()
    {
        return $this->receiverAddress;
    }

    /**
     * @param string $receiverAddress
     * @return self
     */
    public function setReceiverAddress($receiverAddress)
    {
        $this->receiverAddress = $receiverAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getReceiverPhone()
    {
        return $this->receiverPhone;
    }

    /**
     * @param string $receiverPhone
     * @return self
     */
    public function setReceiverPhone($receiverPhone)
    {
        $this->receiverPhone = $receiverPhone;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float
     */
    public function getShippingCost()
    {
        return $this->shippingCost;
    }

    /**
     * @param float $shippingCost
     * @return self
     */
    public function setShippingCost($shippingCost)
    {
        $this->shippingCost = $shippingCost;
        return $this;
    }

    /**
     * @return float
     */
    public function getCod()
    {
        return $this->cod;
    }

    /**
     * @param float $cod
     * @return self
     */
    public function setCod($cod)
    {
        $this->cod = $cod;
        return $this;
    }

    /**
     * @return int
     */
    public function getCodStatus()
    {
        return $this->codStatus;
    }

    /**
     * @param int $codStatus
     * @return self
     */
    public function setCodStatus($codStatus)
    {
        $this->codStatus = $codStatus;
        return $this;
    }

    /**
     * @return int
     */
    public function getShippingStatus()
    {
        return $this->shippingStatus;
    }

    /**
     * @param int $shippingStatus
     * @return self
     */
    public function setShippingStatus($shippingStatus)
    {
        $this->shippingStatus = $shippingStatus;
        return $this;
    }

    /**
     * @return int
     */
    public function getPaidStatus()
    {
        return $this->paidStatus;
    }

    /**
     * @param int $paidStatus
     * @return self
     */
    public function setPaidStatus($paidStatus)
    {
        $this->paidStatus = $paidStatus;
        return $this;
    }

    /**
     * @return UserEntityInterface
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param UserEntityInterface $creator
     * @return self
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
        return $this;
    }

    /**
     * @return DistrictInterface
     */
    public function getTargetDistrict()
    {
        return $this->targetDistrict;
    }

    /**
     * @param DistrictInterface $targetDistrict
     */
    public function setTargetDistrict(DistrictInterface $targetDistrict)
    {
        $this->targetDistrict = $targetDistrict;
    }

    /**
     * @return string
     */
    public function getSenderPhone()
    {
        return $this->senderPhone;
    }

    /**
     * @param string $senderPhone
     * @return self
     */
    public function setSenderPhone($senderPhone)
    {
        $this->senderPhone = $senderPhone;
        return $this;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param float $vat
     * @return self
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * @param float $totalCost
     * @return self
     */
    public function setTotalCost($totalCost)
    {
        $this->totalCost = $totalCost;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isSenderPay()
    {
        return $this->senderPay;
    }

    /**
     * @param boolean $senderPay
     * @return self
     */
    public function setSenderPay($senderPay)
    {
        $this->senderPay = $senderPay;
        return $this;
    }

    /**
     * @return float
     */
    public function getSenderIncome()
    {
        return $this->senderIncome;
    }

    /**
     * @param float $senderIncome
     * @return self
     */
    public function setSenderIncome($senderIncome)
    {
        $this->senderIncome = $senderIncome;
        return $this;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     * @return self
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return float
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param float $height
     * @return self
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return float
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param float $width
     * @return self
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return float
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param float $length
     * @return self
     */
    public function setLength($length)
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return int
     */
    public function getGoodType()
    {
        return $this->goodType;
    }

    /**
     * @param int $goodType
     * @return self
     */
    public function setGoodType($goodType)
    {
        $this->goodType = $goodType;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isUrgent()
    {
        return $this->urgent;
    }

    /**
     * @param boolean $urgent
     * @return self
     */
    public function setUrgent($urgent)
    {
        $this->urgent = $urgent;
        return $this;
    }


    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return self
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return self
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

    /**
     * @return DistrictInterface
     */
    public function getSenderDistrict()
    {
        return $this->senderDistrict;
    }

    /**
     * @param DistrictInterface $senderDistrict
     */
    public function setSenderDistrict($senderDistrict)
    {
        $this->senderDistrict = $senderDistrict;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @param mixed $distance
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 12/04/2016
 * Time: 23:11
 */

namespace DTag\Model\Core;


use DTag\Model\User\Role\ShipperInterface;

class OrderAssignation implements OrderAssignationInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var OrderInterface
     */
    protected $order;

    /**
     * @var ShipperInterface
     */
    protected $shipper;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     */
    protected $deletedAt;


    /**
     * Order constructor.
     */
    public function __construct()
    {
    }

    public function getId()
    {
        return $this->id;
    }


    /**
     * @param int $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @return OrderInterface
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param OrderInterface $order
     * @return self
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return ShipperInterface
     */
    public function getShipper()
    {
        return $this->shipper;
    }

    /**
     * @param ShipperInterface $shipper
     * @return self
     */
    public function setShipper($shipper)
    {
        $this->shipper = $shipper;
        return $this;
    }
}
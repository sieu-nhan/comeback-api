<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 13/04/2016
 * Time: 22:41
 */

namespace DTag\Model\Core;


use DTag\Model\ModelInterface;
use DTag\Model\User\Role\ShipperInterface;

interface OrderAssignationInterface extends ModelInterface
{
    /**
     * @param int $id
     * @return self
     */
    public function setId($id);

    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * @return \DateTime
     */
    public function getDeletedAt();

    /**
     * @return OrderInterface
     */
    public function getOrder();

    /**
     * @param OrderInterface $order
     * @return self
     */
    public function setOrder($order);

    /**
     * @return ShipperInterface
     */
    public function getShipper();

    /**
     * @param ShipperInterface $shipper
     * @return self
     */
    public function setShipper($shipper);
}
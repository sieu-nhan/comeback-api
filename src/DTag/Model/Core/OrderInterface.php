<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 12/04/2016
 * Time: 23:11
 */

namespace DTag\Model\Core;

use DTag\Model\ModelInterface;
use DTag\Model\User\UserEntityInterface;

interface OrderInterface extends ModelInterface
{
    const SHIPPING_STATUS_CREATED = 0;
    const SHIPPING_STATUS_WAIT_FOR_TAKEN = 1;
    const SHIPPING_STATUS_IN_STOCK = 2;
    const SHIPPING_STATUS_ON_DELIVERY = 3;
    const SHIPPING_STATUS_RETURN_TO_CUSTOMER = 4;
    const SHIPPING_STATUS_WAIT_TO_RESEND = 5;
    const SHIPPING_STATUS_CANCELED = 6;
    const SHIPPING_STATUS_DELIVERED = 7;

    const PAID_STATUS_UNPAID = 0;
    const PAID_STATUS_PAID = 1;

    const COD_STATUS_WAIT_FOR_TRANSFER = 1;
    const COD_STATUS_TRANSFERRED = 2;



    /**
     * @param int $id
     * @return self
     */
    public function setId($id);


    /**
     * @return string
     */
    public function getOrderId();

    /**
     * @param string $orderId
     * @return self
     */
    public function setOrderId($orderId);

    /**
     * @return string
     */
    public function getSenderName();

    /**
     * @param string $senderName
     * @return self
     */
    public function setSenderName($senderName);

    /**
     * @return string
     */
    public function getSenderAddress();

    /**
     * @param string $senderAddress
     * @return self
     */
    public function setSenderAddress($senderAddress);

    /**
     * @return string
     */
    public function getReceiverName();

    /**
     * @param string $receiverName
     * @return self
     */
    public function setReceiverName($receiverName);

    /**
     * @return string
     */
    public function getReceiverAddress();

    /**
     * @param string $receiverAddress
     * @return self
     */
    public function setReceiverAddress($receiverAddress);

    /**
     * @return string
     */
    public function getReceiverPhone();

    /**
     * @param string $receiverPhone
     * @return self
     */
    public function setReceiverPhone($receiverPhone);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     * @return self
     */
    public function setDescription($description);

    /**
     * @return float
     */
    public function getShippingCost();

    /**
     * @param float $shippingCost
     * @return self
     */
    public function setShippingCost($shippingCost);

    /**
     * @return float
     */
    public function getCod();

    /**
     * @param float $cod
     * @return self
     */
    public function setCod($cod);

    /**
     * @return int
     */
    public function getCodStatus();

    /**
     * @param int $codStatus
     * @return self
     */
    public function setCodStatus($codStatus);

    /**
     * @return int
     */
    public function getShippingStatus();

    /**
     * @param int $shippingStatus
     * @return self
     */
    public function setShippingStatus($shippingStatus);

    /**
     * @return int
     */
    public function getPaidStatus();

    /**
     * @param int $paidStatus
     * @return self
     */
    public function setPaidStatus($paidStatus);

    /**
     * @return UserEntityInterface
     */
    public function getCreator();

    /**
     * @param UserEntityInterface $creator
     * @return self
     */
    public function setCreator($creator);

    /**
     * @return DistrictInterface
     */
    public function getTargetDistrict();

    /**
     * @param DistrictInterface $targetDistrict
     */
    public function setTargetDistrict(DistrictInterface $targetDistrict);

    /**
     * @return string
     */
    public function getSenderPhone();

    /**
     * @param string $senderPhone
     * @return self
     */
    public function setSenderPhone($senderPhone);

    /**
     * @return float
     */
    public function getVat();

    /**
     * @param float $vat
     * @return self
     */
    public function setVat($vat);

    /**
     * @return float
     */
    public function getTotalCost();

    /**
     * @param float $totalCost
     * @return self
     */
    public function setTotalCost($totalCost);

    /**
     * @return boolean
     */
    public function isSenderPay();

    /**
     * @param boolean $senderPay
     * @return self
     */
    public function setSenderPay($senderPay);

    /**
     * @return float
     */
    public function getSenderIncome();

    /**
     * @param float $senderIncome
     * @return self
     */
    public function setSenderIncome($senderIncome);

    /**
     * @return float
     */
    public function getWeight();

    /**
     * @param float $weight
     * @return self
     */
    public function setWeight($weight);

    /**
     * @return float
     */
    public function getHeight();

    /**
     * @param float $height
     * @return self
     */
    public function setHeight($height);

    /**
     * @return float
     */
    public function getWidth();

    /**
     * @param float $width
     * @return self
     */
    public function setWidth($width);

    /**
     * @return float
     */
    public function getLength();

    /**
     * @param float $length
     * @return self
     */
    public function setLength($length);

    /**
     * @return int
     */
    public function getGoodType();

    /**
     * @param int $goodType
     * @return self
     */
    public function setGoodType($goodType);

    /**
     * @return boolean
     */
    public function isUrgent();

    /**
     * @param boolean $urgent
     * @return self
     */
    public function setUrgent($urgent);

    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * @return \DateTime
     */
    public function getDeletedAt();

    /**
     * @return DistrictInterface
     */
    public function getSenderDistrict();

    /**
     * @param DistrictInterface $senderDistrict
     */
    public function setSenderDistrict($senderDistrict);

    /**
     * @return mixed
     */
    public function getDistance();

    /**
     * @param mixed $distance
     */
    public function setDistance($distance);
}
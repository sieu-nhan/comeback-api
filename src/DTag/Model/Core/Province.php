<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 13/04/2016
 * Time: 22:41
 */

namespace DTag\Model\Core;


class Province implements ProvinceInterface
{
    /**
     * @var integer
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $shortName;
    /**
     * @var DistrictInterface[]
     */
    protected $districts;

    /**
     * @var integer
     */
    protected $type;

    /**
     * Province constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @param string $shortName
     * @return self
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
        return $this;
    }

    /**
     * @return DistrictInterface[]
     */
    public function getDistricts()
    {
        return $this->districts;
    }

    /**
     * @param DistrictInterface[] $districts
     * @return self
     */
    public function setDistricts($districts)
    {
        $this->districts = $districts;
        return $this;
    }

    /**
     * @param DistrictInterface $district
     * @return $this
     */
    public function addDistrict(DistrictInterface $district)
    {
        if ($this->districts === null) {
            $this->districts = [];
        }

        $this->districts[] = $district;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
}
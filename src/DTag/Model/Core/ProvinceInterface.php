<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 13/04/2016
 * Time: 22:41
 */

namespace DTag\Model\Core;


use DTag\Model\ModelInterface;

interface ProvinceInterface extends ModelInterface
{
    const PROVINCE_TYPE_CITY = 0;
    const PROVINCE_TYPE_1 = 1;
    const PROVINCE_TYPE_2 = 2;
    const PROVINCE_TYPE_3 = 3;

    const PROVINCE_TYPE_1_COST = 36000;
    const PROVINCE_TYPE_2_COST = 45000;
    const PROVINCE_TYPE_3_COST = 54000;

    /**
     * @param int $id
     * @return self
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return self
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getShortName();

    /**
     * @param string $shortName
     * @return self
     */
    public function setShortName($shortName);

    /**
     * @return DistrictInterface[]
     */
    public function getDistricts();

    /**
     * @param DistrictInterface[] $districts
     * @return self
     */
    public function setDistricts($districts);

    /**
     * @param DistrictInterface $district
     * @return $this
     */
    public function addDistrict(DistrictInterface $district);

    /**
     * @return int
     */
    public function getType();

    /**
     * @param int $type
     * @return self
     */
    public function setType($type);
}
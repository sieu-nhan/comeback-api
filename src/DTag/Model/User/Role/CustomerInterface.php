<?php

namespace DTag\Model\User\Role;

use DTag\Model\Core\DistrictInterface;

interface CustomerInterface extends UserRoleInterface{

    /**
     * @return string
     */
    public function getName();

    /**
     * @param mixed $name
     * @return self
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getPhone();

    /**
     * @param mixed $phone
     * @return self
     */
    public function setPhone($phone);

    /**
     * @return DistrictInterface
     */
    public function getDistrict();

    /**
     * @param DistrictInterface $district
     * @return integer
     */
    public function setDistrict(DistrictInterface $district);

    /**
     * @return string
     */
    public function getAddress();

    /**
     * @param mixed $address
     * @return self
     */
    public function setAddress($address);

    /**
     * @return bool
     */
    public function getActive();

    /**
     * @param mixed $active
     * @return self
     */
    public function setActive($active);

    /**
     * @return int
     */
    public function getGender();

    /**
     * @param int $gender
     * @return self
     */
    public function setGender($gender);

    /**
     * @return boolean
     */
    public function isPersonal();

    /**
     * @param boolean $personal
     * @return self
     */
    public function setPersonal($personal);

    /**
     * @return string
     */
    public function getWard();

    /**
     * @param mixed $ward
     * @return self
     */
    public function setWard($ward);

    /**
     * @return string
     */
    public function getAccountNumber();

    /**
     * @param string $accountNumber
     * @return self
     */
    public function setAccountNumber($accountNumber);

    /**
     * @return string
     */
    public function getAccountOwner();

    /**
     * @param string $accountOwner
     * @return self
     */
    public function setAccountOwner($accountOwner);

    /**
     * @return string
     */
    public function getBankName();

    /**
     * @param string $bankName
     * @return self
     */
    public function setBankName($bankName);

    /**
     * @return string
     */
    public function getBranch();

    /**
     * @param string $branch
     * @return self
     */
    public function setBranch($branch);

    /**
     * @return string
     */
    public function getCustomerCode();

    /**
     * @param string $customerCode
     * @return self
     */
    public function setCustomerCode($customerCode);
} 
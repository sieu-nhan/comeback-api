<?php

namespace DTag\Model\User\Role;

interface ManagerInterface extends UserRoleInterface
{
    /**
     * @return string
     */
    public function getPhone();

    /**
     * @param mixed $phone
     */
    public function setPhone($phone);

    /**
     * @return string
     */
    public function getAddress();

    /**
     * @param mixed $address
     */
    public function setAddress($address);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return self
     */
    public function setName($name);

    /**
     * @return int
     */
    public function getGender();

    /**
     * @param int $gender
     * @return self
     */
    public function setGender($gender);
} 
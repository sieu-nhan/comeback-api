<?php

namespace DTag\Model\User\Role;

interface ShipperInterface extends UserRoleInterface{

    /**
     * @return string
     */
    public function getName();

    /**
     * @param mixed $name
     * @return self
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getPhone();

    /**
     * @param mixed $phone
     * @return self
     */
    public function setPhone($phone);

    /**
     * @return string
     */
    public function getAddress();

    /**
     * @param mixed $address
     * @return self
     */
    public function setAddress($address);
} 
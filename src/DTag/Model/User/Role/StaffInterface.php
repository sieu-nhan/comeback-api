<?php

namespace DTag\Model\User\Role;

interface StaffInterface extends UserRoleInterface{

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @param mixed $name
     * @return self
     */
    public function setName($name);

    /**
     * @return mixed
     */
    public function getPhone();

    /**
     * @param mixed $phone
     * @return self
     */
    public function setPhone($phone);

    /**
     * @return mixed
     */
    public function getAddress();

    /**
     * @param mixed $address
     * @return self
     */
    public function setAddress($address);

    /**
     * @return mixed
     */
    public function getActive();

    /**
     * @param mixed $active
     * @return self
     */
    public function setActive($active);

    /**
     * @return int
     */
    public function getGender();

    /**
     * @param int $gender
     * @return self
     */
    public function setGender($gender);

    /**
     * @return string
     */
    public function getPosition();

    /**
     * @param string $position
     * @return self
     */
    public function setPosition($position);

    /**
     * @return string
     */
    public function getGroup();

    /**
     * @param string $group
     * @return self
     */
    public function setGroup($group);
} 
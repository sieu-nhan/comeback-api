<?php

namespace DTag\Repository\Core;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use DTag\Model\Core\DistrictInterface;

class DistrictRepository extends EntityRepository implements DistrictRepositoryInterface
{
    public function findByDistrictShortNameAndProvinceShortName($districtShortName, $provinceShortName)
    {
        return $this->createQueryBuilder('d')
            ->join('d.province', 'p')
            ->where('d.shortName = :districtShortName')
            ->andWhere('p.shortName = :provinceShortName')
            ->setParameter('districtShortName', $districtShortName)
            ->setParameter('provinceShortName', $provinceShortName)
            ->getQuery()->getOneOrNullResult();
    }
}
<?php

namespace DTag\Repository\Core;

use Doctrine\Common\Persistence\ObjectRepository;
use DTag\Model\Core\DistrictInterface;

interface DistrictRepositoryInterface extends ObjectRepository
{
    /**
     * @param $districtShortName
     * @param $provinceShortName
     * @return DistrictInterface|null
     */
    public function findByDistrictShortNameAndProvinceShortName($districtShortName, $provinceShortName);
}
<?php

namespace DTag\Repository\Core;

use Doctrine\ORM\EntityRepository;
use DTag\Model\Core\DistrictInterface;

class InterDistrictServiceCostRepository extends EntityRepository implements InterDistrictServiceCostRepositoryInterface
{
    public function findByDistrict(DistrictInterface $district)
    {
        return $this->createQueryBuilder('sc')
            ->where('sc.district = :district')
            ->setParameter('district', $district)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
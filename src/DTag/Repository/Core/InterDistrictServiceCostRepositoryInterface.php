<?php

namespace DTag\Repository\Core;

use Doctrine\Common\Persistence\ObjectRepository;
use DTag\Entity\Core\InterDistrictServiceCost;
use DTag\Model\Core\DistrictInterface;

interface InterDistrictServiceCostRepositoryInterface extends ObjectRepository
{
    /**
     * @param DistrictInterface $district
     * @return InterDistrictServiceCost
     */
    public function findByDistrict(DistrictInterface $district);
}
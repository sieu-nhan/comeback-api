<?php

namespace DTag\Repository\Core;

use Doctrine\Common\Persistence\ObjectRepository;

interface ManagerRepositoryInterface extends ObjectRepository
{
} 
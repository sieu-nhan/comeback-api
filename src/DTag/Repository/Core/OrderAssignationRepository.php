<?php

namespace DTag\Repository\Core;

use Doctrine\ORM\EntityRepository;
use DTag\Model\Core\OrderAssignationInterface;
use DTag\Model\Core\OrderInterface;

class OrderAssignationRepository extends EntityRepository implements OrderAssignationRepositoryInterface
{
    /**
     * @param OrderInterface $order
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function isOrderAssigned(OrderInterface $order)
    {
        return $this->createQueryBuilder('oa')
            ->where('oa.order = :order')
            ->setParameter('order', $order)
            ->getQuery()
            ->getOneOrNullResult() instanceof OrderAssignationInterface;
    }

}
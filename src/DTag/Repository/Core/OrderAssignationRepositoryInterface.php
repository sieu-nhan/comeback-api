<?php

namespace DTag\Repository\Core;

use Doctrine\Common\Persistence\ObjectRepository;
use DTag\Model\Core\OrderInterface;

interface OrderAssignationRepositoryInterface extends ObjectRepository
{
    /**
     * @param OrderInterface $order
     * @return boolean
     */
    public function isOrderAssigned(OrderInterface $order);
}
<?php

namespace DTag\Repository\Core;

use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\Types\Type;
use DTag\Model\Core\OrderInterface;
use DTag\Model\User\Role\CustomerInterface;
use DTag\Model\User\Role\ManagerInterface;
use DTag\Model\User\Role\StaffInterface;
use DTag\Model\User\Role\UserRoleInterface;
use Sortable\Fixture\Customer;

class OrderRepository extends EntityRepository implements OrderRepositoryInterface
{
    /**
     * @param CustomerInterface $customer
     * @param null $limit
     * @param null $offset
     * @return array
     */
    public function getOrderForCustomer(CustomerInterface $customer, $limit = null, $offset = null)
    {
        $qb = $this->createQueryBuilder('o')
            ->where('o.creator = :creator')
            ->setParameter('creator', $customer);

        if (is_int($limit)) {
            $qb->setMaxResults($limit);
        }

        if (is_int($offset)) {
            $qb->setFirstResult($offset);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param StaffInterface $staff
     * @param null $limit
     * @param null $offset
     * @return array
     */
    public function getOrderForStaff(StaffInterface $staff, $limit = null, $offset = null)
    {
        $qb = $this->createQueryBuilder('o')
            ->where('o.creator = :creator')
            ->setParameter('creator', $staff);

        if (is_int($limit)) {
            $qb->setMaxResults($limit);
        }

        if (is_int($offset)) {
            $qb->setFirstResult($offset);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param UserRoleInterface $user
     * @param $status
     * @param null $startDate
     * @param null $endDate
     * @return array
     */
    public function getOrderByShippingStatus(UserRoleInterface $user, $status, $startDate = null, $endDate = null)
    {
        $qb = $this->createQueryBuilder('o');

        if($user instanceof CustomerInterface) {
            $qb ->join('o.creator', 'creator')
                ->where('creator.id = :creator_id')
                ->setParameter('creator_id', $user->getId());
        }

        if(!!$status) {
            $qb ->andWhere('o.shippingStatus = :status')
                ->setParameter('status', $status);
        }

        if ($startDate instanceof \DateTime) {
            if (!$endDate instanceof \DateTime) {
                $endDate = date("Y-m-d H:i:s");
            }

            $qb->andWhere('o.createdAt BETWEEN :startDate AND :endDate')
                ->setParameter('startDate', $startDate)
                ->setParameter('endDate', $endDate->format('Y-m-d 23:59:59'));
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $status
     * @param null $startDate
     * @param null $endDate
     * @return array
     */
    public function getOrderByPaidStatus($status, $startDate = null, $endDate = null)
    {
        $qb = $this->createQueryBuilder('o')
            ->where('o.paidStatus = :status')
            ->setParameter('status', $status);

        if ($startDate instanceof \DateTime) {
            if (!$endDate instanceof \DateTime) {
                $endDate = date("Y-m-d H:i:s");
            }

            $qb->andWhere('o.createdAt BETWEEN :startDate AND :endDate')
                ->setParameter('startDate', $startDate)
                ->setParameter('endDate', $endDate);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $status
     * @param null $startDate
     * @param null $endDate
     * @return array
     */
    public function getOrderByCodStatus($status, $startDate = null, $endDate = null)
    {
        $qb = $this->createQueryBuilder('o')
            ->where('o.codStatus = :status')
            ->setParameter('status', $status);

        if ($startDate instanceof \DateTime) {
            if (!$endDate instanceof \DateTime) {
                $endDate = date("Y-m-d H:i:s");
            }

            $qb->andWhere('o.createdAt BETWEEN :startDate AND :endDate')
                ->setParameter('startDate', $startDate)
                ->setParameter('endDate', $endDate);
        }

        return $qb->getQuery()->getResult();
    }

    public function getReportByDay(DateTime $date)
    {
        return  $this->createQueryBuilder('o')
            ->join('o.creator', 'customer')
            ->where('customer INSTANCE OF DTag\Bundles\UserSystem\CustomerBundle\Entity\User')
            ->andWhere('DATE_DIFF(o.createdAt, :date) = 0')
            ->andWhere('o.shippingStatus = :status')
            ->setParameter('status', OrderInterface::SHIPPING_STATUS_DELIVERED)
            ->setParameter('date', $date, Type::DATE)
            ->groupBy('o.creator')
            ->select('count(o.id) as number')
            ->addSelect('o.createdAt as date')
            ->addSelect('o.senderName as name')
            ->addSelect('o.senderAddress as address')
            ->addSelect('o.senderPhone as phone')
            ->addSelect('SUM(o.totalCost) as totalCost')
            ->addSelect('SUM(o.shippingCost) as shippingCost')
            ->addSelect('SUM(o.cod) as cod')
            ->addSelect('SUM(o.senderIncome) as senderIncome')
            ->getQuery()
            ->getScalarResult();
    }


}
<?php

namespace DTag\Repository\Core;

use DateTime;
use Doctrine\Common\Persistence\ObjectRepository;
use DTag\Model\User\Role\CustomerInterface;
use DTag\Model\User\Role\ManagerInterface;
use DTag\Model\User\Role\StaffInterface;
use DTag\Model\User\Role\UserRoleInterface;

interface OrderRepositoryInterface extends ObjectRepository
{
    /**
     * @param CustomerInterface $customer
     * @param null $limit
     * @param null $offset
     * @return mixed
     */
    public function getOrderForCustomer(CustomerInterface $customer, $limit = null, $offset = null);

    /**
     * @param StaffInterface $staff
     * @param null $limit
     * @param null $offset
     * @return mixed
     */
    public function getOrderForStaff(StaffInterface $staff, $limit = null, $offset = null);

    /**
     * @param UserRoleInterface $user
     * @param $status
     * @param null $startDate
     * @param null $endDate
     * @return mixed
     */
    public function getOrderByShippingStatus(UserRoleInterface $user, $status, $startDate = null, $endDate = null);

    /**
     * @param $status
     * @param null $startDate
     * @param null $endDate
     * @return mixed
     */
    public function getOrderByPaidStatus($status, $startDate = null, $endDate = null);

    /**]
     * @param $status
     * @param null $startDate
     * @param null $endDate
     * @return mixed
     */
    public function getOrderByCodStatus($status, $startDate = null, $endDate = null);


    public function getReportByDay(DateTime $date);
}
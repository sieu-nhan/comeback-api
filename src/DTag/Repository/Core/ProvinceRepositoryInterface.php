<?php

namespace DTag\Repository\Core;

use Doctrine\Common\Persistence\ObjectRepository;

interface ProvinceRepositoryInterface extends ObjectRepository
{
}
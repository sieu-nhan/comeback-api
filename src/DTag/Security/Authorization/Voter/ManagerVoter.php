<?php

namespace DTag\Security\Authorization\Voter;

use DTag\Model\ModelInterface;
use DTag\Model\User\Role\ManagerInterface;
use DTag\Model\User\UserEntityInterface;

class ManagerVoter extends EntityVoterAbstract
{
    /**
     * Checks to see if a manager has permission to perform an action
     *
     * @param ModelInterface $entity
     * @param UserEntityInterface $user
     * @param $action
     * @return bool
     */
    protected function isManagerActionAllowed(ModelInterface $entity, UserEntityInterface $user, $action)
    {
        return $entity->getId() == $user->getId();
    }

    /**
     * Checks to see if a customer has permission to perform an action
     *
     * @param ModelInterface $entity
     * @param UserEntityInterface $user
     * @param $action
     * @return bool
     */
    protected function isCustomerActionAllowed(ModelInterface $entity, UserEntityInterface $user, $action)
    {
        return false;
    }

    /**
     * Checks if the voter supports the given class.
     *
     * @param string $class A class name
     *
     * @return bool true if this Voter can process the class
     */
    public function supportsClass($class)
    {
        $supportedClass = ManagerInterface::class;

        return $supportedClass === $class || is_subclass_of($class, $supportedClass);
    }
}
<?php

namespace DTag\Security\Authorization\Voter;

use DTag\Model\Core\OrderInterface;
use DTag\Model\ModelInterface;
use DTag\Model\User\Role\ManagerInterface;
use DTag\Model\User\UserEntityInterface;

class OrderVoter extends EntityVoterAbstract
{
    public function supportsClass($class)
    {
        $supportedClass = OrderInterface::class;

        return $supportedClass === $class || is_subclass_of($class, $supportedClass);
    }

    /**
     * Checks to see if a manager has permission to perform an action
     *
     * @param ModelInterface $entity
     * @param UserEntityInterface $user
     * @param $action
     * @return bool
     */
    protected function isManagerActionAllowed(ModelInterface $entity, UserEntityInterface $user, $action)
    {
        /**
         * @var OrderInterface $entity
         * @var ManagerInterface $user
         */
        return true;
    }

    /**
     * Checks to see if a customer has permission to perform an action
     *
     * @param ModelInterface $entity
     * @param UserEntityInterface $user
     * @param $action
     * @return bool
     */
    protected function isCustomerActionAllowed(ModelInterface $entity, UserEntityInterface $user, $action)
    {
        /**
         * @var OrderInterface $entity
         * @var \DTag\Model\User\Role\CustomerInterface $user
         */
        return $entity->getCreator()->getId() == $user->getId();
    }


}
<?php


namespace DTag\Services\Core;


use DTag\Bundles\UserBundle\DomainManager\ShipperManagerInterface;
use DTag\DomainManager\OrderAssignationManagerInterface;
use DTag\DomainManager\OrderManagerInterface;
use DTag\Entity\Core\OrderAssignation;
use DTag\Exception\InvalidArgumentException;
use DTag\Model\Core\OrderInterface;
use DTag\Model\User\Role\ShipperInterface;

class AssignOrderService implements AssignOrderServiceInterface
{
    /**
     * @var OrderManagerInterface
     */
    protected $orderManager;

    /**
     * @var OrderAssignationManagerInterface
     */
    protected $orderAssignationManager;

    /**
     * AssignOrderService constructor.
     * @param OrderManagerInterface $orderManager
     * @param OrderAssignationManagerInterface $orderAssignationManager
     */
    public function __construct(OrderManagerInterface $orderManager, OrderAssignationManagerInterface $orderAssignationManager)
    {
        $this->orderManager = $orderManager;
        $this->orderAssignationManager = $orderAssignationManager;
    }

    public function assign(ShipperInterface $shipper, array $orderIds)
    {
        foreach($orderIds as $orderId) {
            $this->assignSingleOrder($shipper, $orderId);
        }
    }

    protected function assignSingleOrder(ShipperInterface $shipper, $orderId)
    {
        $order = $this->orderManager->find($orderId);
        if (!$order instanceof OrderInterface) {
            throw new InvalidArgumentException(sprintf('order "%d" does not exist or you do not have enough permission to view this entity', $orderId));
        }

        if ($this->orderAssignationManager->isOrderAssigned($order) === true) {
            throw new InvalidArgumentException(sprintf('order "%d" has already been assigned', $orderId));
        }

        $orderAssignation = new OrderAssignation();
        $orderAssignation->setOrder($order)
            ->setShipper($shipper);

        $this->orderAssignationManager->save($orderAssignation);
    }
}
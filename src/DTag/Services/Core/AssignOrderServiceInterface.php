<?php


namespace DTag\Services\Core;


use DTag\Model\User\Role\ShipperInterface;

interface AssignOrderServiceInterface
{
    /**
     * @param ShipperInterface $shipper
     * @param array $orderIds
     * @return mixed
     */
    public function assign(ShipperInterface $shipper, array $orderIds);
}
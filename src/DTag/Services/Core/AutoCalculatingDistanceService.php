<?php
namespace DTag\Services\Core;

use DTag\Model\Core\DistrictInterface;

class AutoCalculatingDistanceService implements AutoCalculatingDistanceServiceInterface
{
    public function AutoCalculatingDistance(DistrictInterface $targetDistrict, $receiverAddress, DistrictInterface $senderDistrict, $senderAddress)
    {
        $targetAddress = $receiverAddress . ', ' . $targetDistrict->getName() . ', ' .  $targetDistrict->getProvince()->getName();
        $coordinatesTargetAddress = $this->GetCoordinatesAddress($targetAddress);

        $receiverAddress = $senderAddress . ', ' . $senderDistrict->getName() . ', ' .  $senderDistrict->getProvince()->getName();
        $coordinatesReceiverAddress = $this->GetCoordinatesAddress($receiverAddress);

        return $this->CalculatingDistance($coordinatesTargetAddress, $coordinatesReceiverAddress);
    }

    protected function GetCoordinatesAddress($address) {
        $address = str_replace(" ", "+", $address);

        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=AIzaSyB_HHUGTOsUM3N8qL8osy84G12tIzF0v04";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);


        $lat = $response_a['results'][0]['geometry']['location']['lat'];
        $long = $response_a['results'][0]['geometry']['location']['lng'];

        return $lat.','.$long;
    }

    protected function CalculatingDistance($coordinatesTargetAddress, $coordinatesReceiverAddress) {
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$coordinatesTargetAddress."&destinations=".$coordinatesReceiverAddress."&mode=driving&language=pl-PL";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);

        // get km
        return $response_a['rows'][0]['elements'][0]['distance']['value']/1000;
    }
}
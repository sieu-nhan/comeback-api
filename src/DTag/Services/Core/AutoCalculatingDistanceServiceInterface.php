<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 4/17/16
 * Time: 10:40 AM
 */

namespace DTag\Services\Core;

use DTag\Model\Core\DistrictInterface;

interface AutoCalculatingDistanceServiceInterface
{
    public function AutoCalculatingDistance(DistrictInterface $targetDistrict, $receiverAddress, DistrictInterface $senderDistrict, $senderAddress);
}
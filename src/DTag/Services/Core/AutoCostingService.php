<?php

namespace DTag\Services\Core;

use DTag\Exception\InvalidArgumentException;
use DTag\Model\Core\DistrictInterface;
use DTag\Model\Core\OrderInterface;
use DTag\Model\Core\ProvinceInterface;
use DTag\Repository\Core\InterDistrictServiceCostRepositoryInterface;

class AutoCostingService implements AutoCostingServiceInterface
{
    const DISTANCE_THRESHOLD = 10; // km
    const COST_BLOCK = 40000;  // VND
    const COST_PER_KM = 3000; // VND

    const CITY_WEIGHT_THRESHOLD = 2;
    const CITY_COST_PER_UNIT_OVER = 3000; // VND

    const SUBURBAN_WEIGHT_THRESHOLD = 2;
    const SUBURBAN_WEIGHT_URGENT = 2;
    const SUBURBAN_COST_PER_UNIT_OVER = 10000;

    /**
     * @var InterDistrictServiceCostRepositoryInterface
     */
    protected $interDistrictServiceCostRepository;

    public function setInterDistrictServiceCostRepository(InterDistrictServiceCostRepositoryInterface $interDistrictServiceCostRepository)
    {
        $this->interDistrictServiceCostRepository = $interDistrictServiceCostRepository;
    }

    public function calculateServiceCostNormal(DistrictInterface $targetDistrict, $weight)
    {
        $district = $targetDistrict;
        $province = $district->getProvince();

        $shippingCost = 0;

        $districtServiceCost = $this->interDistrictServiceCostRepository->findByDistrict($district);
        $shippingCost += !!$districtServiceCost ? $districtServiceCost->getCost() : 0;

        if ($province->getType() === ProvinceInterface::PROVINCE_TYPE_CITY) {
            if(ceil($weight - self::CITY_WEIGHT_THRESHOLD) > 0) {
                $shippingCost += (ceil($weight - self::CITY_WEIGHT_THRESHOLD) * self::CITY_COST_PER_UNIT_OVER);

                return $shippingCost;
            }

            return $shippingCost;
        } else if ($province->getType() === ProvinceInterface::PROVINCE_TYPE_1) {
            if(ceil($weight - self::SUBURBAN_WEIGHT_THRESHOLD)/2 > 0) {
                return (((ceil($weight - self::SUBURBAN_WEIGHT_THRESHOLD)/2) * self::SUBURBAN_COST_PER_UNIT_OVER) + ProvinceInterface::PROVINCE_TYPE_1_COST);
            }

            return ProvinceInterface::PROVINCE_TYPE_1_COST;
        } else if ($province->getType() === ProvinceInterface::PROVINCE_TYPE_2) {
            if(ceil($weight - self::SUBURBAN_WEIGHT_THRESHOLD)/2 > 0) {
                return (((ceil($weight - self::SUBURBAN_WEIGHT_THRESHOLD)/2) * self::SUBURBAN_COST_PER_UNIT_OVER) + ProvinceInterface::PROVINCE_TYPE_2_COST);
            }

            return ProvinceInterface::PROVINCE_TYPE_2_COST;
        } else if ($province->getType() === ProvinceInterface::PROVINCE_TYPE_3) {
            if(ceil($weight - self::SUBURBAN_WEIGHT_THRESHOLD)/2 > 0) {
                return (((ceil($weight - self::SUBURBAN_WEIGHT_THRESHOLD)/2) * self::SUBURBAN_COST_PER_UNIT_OVER) + ProvinceInterface::PROVINCE_TYPE_2_COST);
            }

            return ProvinceInterface::PROVINCE_TYPE_2_COST;
        }
    }

    public function calculateServiceCostUrgent(DistrictInterface $targetDistrict, $distance, $weight) {
        $district = $targetDistrict;
        $province = $district->getProvince();

        if ($province->getType() === ProvinceInterface::PROVINCE_TYPE_CITY) {
            if(ceil($distance - self::DISTANCE_THRESHOLD) > 0) {
                $shippingCost = ((ceil($distance - self::DISTANCE_THRESHOLD) * self::CITY_COST_PER_UNIT_OVER) + self::COST_BLOCK);
                if(ceil($weight - self::SUBURBAN_WEIGHT_URGENT) > 0) {
                    $shippingCost += (ceil($weight - self::SUBURBAN_WEIGHT_URGENT) * self::CITY_COST_PER_UNIT_OVER);

                    return $shippingCost;
                }

                return $shippingCost;
            }

            $shippingCost = self::COST_BLOCK;
            if(ceil($weight - self::SUBURBAN_WEIGHT_URGENT) > 0) {
                $shippingCost += (ceil($weight - self::SUBURBAN_WEIGHT_URGENT) * self::CITY_COST_PER_UNIT_OVER);

                return $shippingCost;
            }

            return $shippingCost;
        } else {
            return 0;
        }
    }
}
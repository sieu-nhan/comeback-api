<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 4/17/16
 * Time: 10:40 AM
 */

namespace DTag\Services\Core;


use DTag\Model\Core\DistrictInterface;
use DTag\Model\Core\OrderInterface;
use DTag\Repository\Core\InterDistrictServiceCostRepositoryInterface;

interface AutoCostingServiceInterface
{
    public function calculateServiceCostNormal(DistrictInterface $targetDistrict, $weight);

    public function calculateServiceCostUrgent(DistrictInterface $targetDistrict, $distance, $weight);

    public function setInterDistrictServiceCostRepository(InterDistrictServiceCostRepositoryInterface $interDistrictServiceCostRepository);
}
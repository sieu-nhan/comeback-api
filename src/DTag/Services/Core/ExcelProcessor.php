<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 18/04/2016
 * Time: 21:43
 */

namespace DTag\Services\Core;
use DTag\DomainManager\OrderManagerInterface;
use DTag\Entity\Core\Order;
use DTag\Model\Core\DistrictInterface;
use DTag\Repository\Core\DistrictRepositoryInterface;
use Liuggio\ExcelBundle\Factory;

class ExcelProcessor implements ExcelProcessorInterface
{

    const ID_INDEX = 0;
    const SENDER_NAME_INDEX = 1;
    const SENDER_ADDRESS_INDEX = 2;
    const SENDER_PHONE_INDEX = 3;
    const SENDER_DISTRICT_INDEX = 4;
    const SENDER_PROVINCE_INDEX = 5;
    const RECEIVER_PHONE_INDEX = 6;
    const RECEIVER_ADDRESS_INDEX = 7;
    const RECEIVER_NAME_INDEX = 8;
    const RECEIVER_DISTRICT_INDEX = 9;
    const RECEIVER_PROVINCE_INDEX = 10;
    const WEIGHT_INDEX = 11;
    const LENGTH_INDEX = 12;
    const WIDTH_INDEX = 13;
    const HEIGHT_INDEX = 14;
    const URGENT_INDEX = 15;
    const COD_INDEX = 16;
    const SENDER_PAY_INDEX = 17;
    const SHIPPING_STATUS_INDEX = 18;
    const PAID_STATUS_INDEX = 19;
    const COD_STATUS_INDEX = 20;
    const DESCRIPTION_INDEX = 21;

    /**
     * @var Factory
     */
    protected $excelFactory;

    /**
     * @var OrderManagerInterface
     */
    protected $orderManager;

    /**
     * @var DistrictRepositoryInterface
     */
    protected $districtRepository;
    /**
     * ExcelProcessor constructor.
     * @param Factory $excelFactory
     * @param OrderManagerInterface $orderManager
     * @param DistrictRepositoryInterface $districtRepository
     */
    public function __construct(Factory $excelFactory, OrderManagerInterface $orderManager, DistrictRepositoryInterface $districtRepository)
    {
        $this->excelFactory = $excelFactory;
        $this->orderManager = $orderManager;
        $this->districtRepository = $districtRepository;
    }


    public function createOrdersFromFile($filePath)
    {
        $excelFile = $this->excelFactory->createPHPExcelObject($filePath);
        $mySheet = $excelFile->getActiveSheet();
        $highestRow = $mySheet->getHighestRow();
        $highestColumn = $mySheet->getHighestColumn();

        $count = 0;
        //  Loop through each row of the worksheet in turn
        for ($row = 2; $row <= $highestRow; $row++){
            //  Read a row of data into an array
            $rowData = $mySheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            $order = new Order();
            $order
                ->setOrderId(strtoupper(uniqid("")))
                ->setSenderName($rowData[0][self::SENDER_NAME_INDEX])
                ->setSenderAddress($rowData[0][self::SENDER_ADDRESS_INDEX])
                ->setSenderPhone($rowData[0][self::SENDER_PHONE_INDEX])
                ->setReceiverName($rowData[0][self::RECEIVER_NAME_INDEX])
                ->setReceiverAddress($rowData[0][self::RECEIVER_ADDRESS_INDEX])
                ->setReceiverPhone($rowData[0][self::RECEIVER_PHONE_INDEX])
                ->setWeight($rowData[0][self::WEIGHT_INDEX])
                ->setHeight($rowData[0][self::HEIGHT_INDEX])
                ->setWidth($rowData[0][self::WIDTH_INDEX])
                ->setLength($rowData[0][self::LENGTH_INDEX])
                ->setCod($rowData[0][self::COD_INDEX])
                ->setSenderPay(filter_var($rowData[0][self::SENDER_PAY_INDEX], FILTER_VALIDATE_BOOLEAN))
                ->setUrgent(filter_var($rowData[0][self::URGENT_INDEX], FILTER_VALIDATE_BOOLEAN))
                ->setDescription($rowData[0][self::DESCRIPTION_INDEX])
                ->setPaidStatus(intval($rowData[0][self::PAID_STATUS_INDEX]))
                ->setShippingStatus(intval($rowData[0][self::SHIPPING_STATUS_INDEX]))
                ->setCodStatus(intval($rowData[0][self::COD_STATUS_INDEX]))
            ;

            $from = $this->districtRepository->findByDistrictShortNameAndProvinceShortName(
                $rowData[0][self::SENDER_DISTRICT_INDEX],
                $rowData[0][self::SENDER_PROVINCE_INDEX]
            );

            if (!$from instanceof DistrictInterface) {
                continue;
            }

            $to = $this->districtRepository->findByDistrictShortNameAndProvinceShortName(
                $rowData[0][self::RECEIVER_DISTRICT_INDEX],
                $rowData[0][self::RECEIVER_PROVINCE_INDEX]
            );

            if (!$to instanceof DistrictInterface) {
                continue;
            }

            $order
                ->setSenderDistrict($from)
                ->setTargetDistrict($to);

            $this->orderManager->save($order);
            $count++;
            unset($order);
        }

        return $count;
    }
}
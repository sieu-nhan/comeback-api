<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 18/04/2016
 * Time: 21:42
 */

namespace DTag\Services\Core;


interface ExcelProcessorInterface
{
    /**
     * @param $filePath
     * @return mixed
     */
    public function createOrdersFromFile($filePath);
}
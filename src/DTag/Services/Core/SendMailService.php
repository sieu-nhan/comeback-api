<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 09/07/2016
 * Time: 21:44
 */

namespace DTag\Services\Core;


class SendMailService implements SendMailServiceInterface
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    public function __construct(\Twig_Environment $twig, \Swift_Mailer $mailer)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
    }

    /**
     * @inheritdoc
     */
    public function send($title, $email, $template, array $params)
    {
        $body = $this->twig->render($template, $params);

        $message = \Swift_Message::newInstance()
            ->setSubject($title)
            ->setFrom('noreply@bocauship.net')
            ->setTo($email)
            ->setBody($body, "text/html", "UTF-8")
        ;

        $this->mailer->send($message);
    }
}
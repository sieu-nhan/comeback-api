<?php
/**
 * Created by PhpStorm.
 * User: giang
 * Date: 09/07/2016
 * Time: 21:44
 */

namespace DTag\Services\Core;


interface SendMailServiceInterface
{
    /**
     * @param $title
     * @param $email
     * @param $template
     * @param array $params
     * @return mixed
     */
    public function send($title, $email, $template, array $params);
}
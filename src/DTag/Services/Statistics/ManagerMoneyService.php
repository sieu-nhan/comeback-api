<?php

namespace DTag\Services\Statistics;

use DTag\Model\Core\OrderInterface;
use DTag\Model\User\Role\CustomerInterface;
use DTag\Model\User\Role\UserRoleInterface;
use DTag\Repository\Core\OrderRepositoryInterface;

class ManagerMoneyService implements ManagerMoneyServiceInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function getStatisticMoney(UserRoleInterface $user) {
        if($user instanceof CustomerInterface) {
            $orderList = $this->orderRepository->getOrderForCustomer($user);
        } else {
            $orderList = $this->orderRepository->findAll();
        }

        return ['paid' => $this->getOrderPaid($orderList), 'unpaid' => $this->getOrderUnpaid($orderList), 'unsent' => $this->getOrderUnsent($orderList)];
    }

    /**
     * @param $orderList
     * @return array
     */
    protected function getOrderPaid($orderList) {
        $count = 0;
        $totalCost = 0;

        /**
         * @var OrderInterface[] $orderList
         */
        foreach($orderList as $order) {
            if($order->getPaidStatus() == true) {
                $count++;
                $totalCost += $order->getTotalCost();
            }
        }

        return ['count' => $count, 'totalCost' => $totalCost];
    }

    /**
     * @param $orderList
     * @return array
     */
    protected function getOrderUnpaid($orderList) {
        $count = 0;
        $totalCost = 0;

        /**
         * @var OrderInterface[] $orderList
         */
        foreach($orderList as $order) {
            if($order->getPaidStatus() == false && $order->getShippingStatus() == 5) {
                $count++;
                $totalCost += $order->getTotalCost();
            }
        }

        return ['count' => $count, 'totalCost' => $totalCost];
    }

    /**
     * @param $orderList
     * @return array
     */
    protected function getOrderUnsent($orderList) {
        $count = 0;
        $totalCost = 0;

        /**
         * @var OrderInterface[] $orderList
         */
        foreach($orderList as $order) {
            if($order->getPaidStatus() == false && $order->getShippingStatus() < 5) {
                $count++;
                $totalCost += $order->getTotalCost();
            }
        }

        return ['count' => $count, 'totalCost' => $totalCost];
    }
}
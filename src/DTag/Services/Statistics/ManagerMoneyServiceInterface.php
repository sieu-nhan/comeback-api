<?php
namespace DTag\Services\Statistics;

use DTag\Model\User\Role\UserRoleInterface;

interface ManagerMoneyServiceInterface
{
    /**
     * @param UserRoleInterface $user
     * @return mixed
     */
    public function getStatisticMoney(UserRoleInterface $user);
}
<?php


namespace DTag\Services\captcha;


use Ci\RestClientBundle\Services\RestClient;
use Symfony\Component\Config\Definition\Exception\Exception;

class ValidateCaptchaService implements ValidateCaptchaServiceInterface
{

    const QUERY_PARAM = '?secret=%s&response=%s';
    private $verifyUrl;
    private $secret;
    /**
     * @var RestClient
     */
    private $restClient;

    function __construct($verifyUrl, $secret, RestClient $restClient)
    {
        $this->verifyUrl = $verifyUrl;
        $this->secret = $secret;
        $this->restClient = $restClient;
    }

    /**
     * @param string $response
     * @return bool
     */
    public function validate($response)
    {
        $this->verifyUrl = $this->verifyUrl . self::QUERY_PARAM;
        $url = sprintf($this->verifyUrl, $this->secret, $response);
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $response_a = json_decode($response, true);

            return filter_var($response_a['success'], FILTER_VALIDATE_BOOLEAN);
        }
        catch(Exception $e) {
            return false;
        }
    }
}
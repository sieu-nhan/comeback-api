<?php


namespace DTag\Services\captcha;


interface ValidateCaptchaServiceInterface
{
    /**
     * @param string $response
     * @return bool
     */
    public function validate($response);
}